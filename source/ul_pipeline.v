/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: Pipeline
 *
 */


`timescale 1 ns / 1 ps
`default_nettype none

module ul_pipeline #(
   CLOCK_CYCLES     = 0,
   DATAWIDTH_BITS   = 0,
   SHREG            = "YES"
)(
   input wire clk_i,
   input wire [DATAWIDTH_BITS-1:0] x,
   output wire [DATAWIDTH_BITS-1:0] y
);

   /* Local parameters, wires & registers */
   integer i;
   reg [DATAWIDTH_BITS-1:0] shift_reg [CLOCK_CYCLES-1:0];
   (* shreg_extract = "no" *) reg [DATAWIDTH_BITS-1:0] pipeline_reg [CLOCK_CYCLES-1:0];


if (SHREG == "YES") begin
   initial begin
      for (i = 0; i < CLOCK_CYCLES; i = i + 1) begin
         shift_reg[i] <= {(DATAWIDTH_BITS){1'b0}};
      end
   end

   always @(posedge clk_i) begin
      shift_reg[0] <= x;
      for (i = 0; i < CLOCK_CYCLES - 1; i = i + 1) begin
         shift_reg[i+1] <= shift_reg[i];
      end
   end

   assign y = shift_reg[CLOCK_CYCLES-1];

end else begin
   initial begin
      for (i = 0; i < CLOCK_CYCLES; i = i + 1) begin
         pipeline_reg[i] <= {(DATAWIDTH_BITS){1'b0}};
      end
   end

   always @(posedge clk_i) begin
      pipeline_reg[0] <= x;
      for (i = 0; i < CLOCK_CYCLES - 1; i = i + 1) begin
         pipeline_reg[i+1] <= pipeline_reg[i];
      end
   end

   assign y = pipeline_reg[CLOCK_CYCLES-1];

end

endmodule

`default_nettype wire
