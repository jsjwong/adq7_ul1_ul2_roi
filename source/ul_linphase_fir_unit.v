/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: Linear-phase FIR filter unit. The output data is subjected to
 *              saturated rounding.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_linphase_fir_unit #(
   DATAWIDTH_BITS   = 0,
   COEFF_WIDTH      = 0,
   ORDER            = 0,
   COEFF_FRAC_WIDTH = 0,
   PIPELINE_CHECKER = 0
)(
   input wire clk_i,
   input wire rst_i,

   input wire [DATAWIDTH_BITS*(ORDER+1)-1:0] data_i,
   input wire data_valid_i,
   input wire [COEFF_WIDTH*(ORDER/2+1)-1:0] coeffs_i,

   output wire [DATAWIDTH_BITS-1:0] data_o,
   output wire data_valid_o
);
   /* Local parameters, wires & registers */
   genvar k;
   localparam AWIDTH = DATAWIDTH_BITS;
   localparam BWIDTH = COEFF_WIDTH;
   localparam CWIDTH = 48;
   localparam DWIDTH = DATAWIDTH_BITS;
   localparam PWIDTH = 48;
   localparam LENGTH = ORDER + 1;
   localparam DWBITS = DATAWIDTH_BITS;

   localparam PIPELINE_INPUT     = 1;
   localparam PIPELINE_ROUND_SAT = 2;
   localparam PIPELINE_ADD_MACC  = 4;
   localparam PIPELINE_DSP       = PIPELINE_ADD_MACC + ORDER / 2;

   localparam PIPELINE = PIPELINE_DSP + PIPELINE_INPUT + PIPELINE_ROUND_SAT;

   if (PIPELINE_CHECKER != PIPELINE)
      always ERROR_LINPHASE_FIR_UNIT_PIPELINE_CHECKER_WRONG_VALUE();

   wire [PWIDTH-1:0] p_out [ORDER/2:0];
   wire [AWIDTH-1:0] a_in [ORDER/2:0];
   wire [BWIDTH-1:0] b_in [ORDER/2:0];
   wire [CWIDTH-1:0] c_in [ORDER/2:0];
   wire [DWIDTH-1:0] d_in [ORDER/2:0];
   wire [ORDER/2:0] data_valid_out;

   reg [PIPELINE_DSP-1:0] data_valid_sr = 'd0;
   wire data_valid_to_saturated_rounding;

   reg [COEFF_WIDTH*(ORDER/2+1)-1:0] coeffs_in;
   reg [DATAWIDTH_BITS*LENGTH-1:0] data_in;
   (* keep = "true" *) reg data_valid_in;

   /* Input register stage */
   always @(posedge clk_i) begin
      coeffs_in     <= coeffs_i;
      data_in       <= data_i;
      data_valid_in <= data_valid_i;
   end

   (* keep = "true", max_fanout = 32 *) reg rst_internal;

   always @(posedge clk_i) begin
      rst_internal <= rst_i;
   end

   generate
      for (k = 0; k < ORDER/2+1; k = k + 1) begin
         if (k == 0) begin
            assign a_in[k] = data_in[k*AWIDTH +: AWIDTH];
            assign c_in[k] = {CWIDTH{1'bX}};
            assign d_in[k] = data_in[AWIDTH*LENGTH-1 - k*AWIDTH -: AWIDTH];

            /* Instantiate ADD->MULT->ACC block */
            ul_add_macc #(
               .AWIDTH (AWIDTH),
               .BWIDTH (BWIDTH),
               .CWIDTH (CWIDTH),
               .DWIDTH (DWIDTH),
               .PWIDTH (PWIDTH),
               .CONFIG ("FIRST"),
               .PIPELINE_CHECKER (PIPELINE_ADD_MACC)
            ) ul_add_macc_inst (
               .clk_i   (clk_i),
               .rst_i   (rst_internal),

               .a_i     (a_in[k]),
               .b_i     (b_in[k]),
               .c_i     (c_in[k]),
               .d_i     (d_in[k]),
               .valid_i (data_valid_in),

               .p_o     (p_out[k]),
               .valid_o (data_valid_out[k])
            );
         end else if (k == ORDER/2) begin
            assign c_in[k] = p_out[k-1];
            if ((ORDER % 2) == 0) begin
               /* Last DSP element is single-input for an even order filter. */
               assign d_in[k] = {(DWIDTH){1'b0}};
               ul_pipeline #(
                  .CLOCK_CYCLES   (k),
                  .DATAWIDTH_BITS (AWIDTH),
                  .SHREG          ("YES")
               ) ul_pipeline_a_inst (
                  .clk_i (clk_i),
                  .x     (data_in[k*AWIDTH +: AWIDTH]),
                  .y     (a_in[k])
               );
            end else begin
               /* Last DSP element is dual-input for an odd order filter. */
               ul_pipeline #(
                  .CLOCK_CYCLES   (k),
                  .DATAWIDTH_BITS (AWIDTH+DWIDTH),
                  .SHREG          ("YES")
               ) ul_pipeline_ad_inst (
                  .clk_i (clk_i),
                  .x     ({data_in[k*AWIDTH +: AWIDTH],
                           data_in[AWIDTH*LENGTH-1 - k*AWIDTH -: AWIDTH]}),
                  .y     ({a_in[k], d_in[k]})
               );
            end

            /* Instantiate ADD->MULT->ACC block */
            ul_add_macc #(
               .AWIDTH (AWIDTH),
               .BWIDTH (BWIDTH),
               .CWIDTH (CWIDTH),
               .DWIDTH (DWIDTH),
               .PWIDTH (PWIDTH),
               .CONFIG ("LAST"),
               .PIPELINE_CHECKER (PIPELINE_ADD_MACC)
            ) ul_add_macc_inst (
               .clk_i   (clk_i),
               .rst_i   (rst_internal),

               .a_i     (a_in[k]),
               .b_i     (b_in[k]),
               .c_i     (c_in[k]),
               .d_i     (d_in[k]),
               .valid_i (data_valid_in),

               .p_o     (p_out[k]),
               .valid_o (data_valid_out[k])
            );
         end else begin
            assign c_in[k] = p_out[k-1];
            ul_pipeline #(
               .CLOCK_CYCLES   (k),
               .DATAWIDTH_BITS (AWIDTH+DWIDTH),
               .SHREG          ("YES")
            ) ul_pipeline_ad_inst (
               .clk_i (clk_i),
               .x     ({data_in[k*AWIDTH +: AWIDTH],
                        data_in[AWIDTH*LENGTH-1 - k*AWIDTH -: AWIDTH]}),
               .y     ({a_in[k], d_in[k]})
            );

            /* Instantiate ADD->MULT->ACC block */
            ul_add_macc #(
               .AWIDTH (AWIDTH),
               .BWIDTH (BWIDTH),
               .CWIDTH (CWIDTH),
               .DWIDTH (DWIDTH),
               .PWIDTH (PWIDTH),
               .CONFIG ("CASCADE"),
               .PIPELINE_CHECKER (PIPELINE_ADD_MACC)
            ) ul_add_macc_inst (
               .clk_i   (clk_i),
               .rst_i   (rst_internal),

               .a_i     (a_in[k]),
               .b_i     (b_in[k]),
               .c_i     (c_in[k]),
               .d_i     (d_in[k]),
               .valid_i (data_valid_in),

               .p_o     (p_out[k]),
               .valid_o (data_valid_out[k])
            );
         end

         assign b_in[k] = coeffs_in[k*COEFF_WIDTH +:COEFF_WIDTH];
      end
   endgenerate

   always @(posedge clk_i) begin
      data_valid_sr <= {data_valid_sr[PIPELINE_DSP-2:0], data_valid_in};
   end

   assign data_valid_to_saturated_rounding = data_valid_sr[PIPELINE_DSP-1];

   /* Saturated rounding */
   ul_saturated_rounding #(
      .INPUT_WIDTH      (PWIDTH),
      .FRAC_WIDTH       (COEFF_FRAC_WIDTH),
      .OUTPUT_WIDTH     (DWBITS),
      .TIE              ("AWAYFROMZERO"),
      .PIPELINE_CHECKER (PIPELINE_ROUND_SAT)
   ) ul_saturated_rounding_inst (
      .clk_i        (clk_i),
      .rst_i        (rst_internal),
      .data_i       (p_out[ORDER/2]),
      .data_valid_i (data_valid_to_saturated_rounding),

      .data_o       (data_o),
      .data_valid_o (data_valid_o)
   );

`ifdef XILINX_SIMULATOR
   wire [DWBITS-1:0] data_i_s [LENGTH-1:0];
   generate
      for (k = 0; k < LENGTH; k = k + 1) begin
         assign data_i_s[k] = data_i[k*DWBITS +: DWBITS];
      end
   endgenerate
`endif

endmodule

`default_nettype wire
