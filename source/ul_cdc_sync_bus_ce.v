/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: CDC synchronization module for a bus. The ce_i input is assumed
 *              to be a strobe signal which is asserted when the input is known
 *              to be stable.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_cdc_sync_bus_ce #(
   WIDTH = 1,
   INIT  = 1'b0
)(
   input wire clk_i,
   input wire ce_i,
   input wire [WIDTH-1:0] data_i,
   output wire [WIDTH-1:0] sync_o,
   output wire ce_o
);

   (* ASYNC_REG = "true" *) reg [WIDTH-1:0] sync_reg = {(WIDTH){INIT}};
   reg ce_reg;

   always @(posedge clk_i) begin
      ce_reg <= ce_i;
      if (ce_i)
         sync_reg <= data_i;
   end

   assign ce_o   = ce_reg;
   assign sync_o = sync_reg;

endmodule

`default_nettype wire
