//Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2017.1 (lin64) Build 1846317 Fri Apr 14 18:54:47 MDT 2017
//Date        : Thu Sep 10 22:38:01 2020
//Host        : spd-eva running 64-bit Ubuntu 18.04.2 LTS
//Command     : generate_target System_wrapper.bd
//Design      : System_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module System_wrapper
   (Vaux0_v_n,
    Vaux0_v_p,
    Vaux1_v_n,
    Vaux1_v_p,
    Vaux8_v_n,
    Vaux8_v_p,
    Vaux9_v_n,
    Vaux9_v_p,
    Vp_Vn_v_n,
    Vp_Vn_v_p,
    a_ddr4_act_n,
    a_ddr4_adr,
    a_ddr4_ba,
    a_ddr4_bg,
    a_ddr4_ck_c,
    a_ddr4_ck_t,
    a_ddr4_cke,
    a_ddr4_dm_n,
    a_ddr4_dq,
    a_ddr4_dqs_c,
    a_ddr4_dqs_t,
    a_ddr4_odt,
    a_ddr4_reset_n,
    a_sys_clk_n,
    a_sys_clk_p,
    adc1_cs_n,
    adc1_d_n,
    adc1_d_p,
    adc1_pdwn,
    adc1_sync_n,
    adc1_sync_p,
    adc2_cs_n,
    adc2_d_n,
    adc2_d_p,
    adc2_pdwn,
    adc2_sync_n,
    adc2_sync_p,
    adc_sclk,
    adc_sdio,
    b_ddr4_act_n,
    b_ddr4_adr,
    b_ddr4_ba,
    b_ddr4_bg,
    b_ddr4_ck_c,
    b_ddr4_ck_t,
    b_ddr4_cke,
    b_ddr4_dm_n,
    b_ddr4_dq,
    b_ddr4_dqs_c,
    b_ddr4_dqs_t,
    b_ddr4_odt,
    b_ddr4_reset_n,
    b_sys_clk_n,
    b_sys_clk_p,
    cfpga_io0_io,
    cfpga_io1_io,
    cfpga_io2_io,
    cfpga_io3_io,
    cfpga_r_w_n,
    cfpga_sck_io,
    cfpga_ss_io,
    clkbufout_en_n,
    cpll_cs_n,
    dac_bias_cs_n,
    db_pwr_gd_n,
    ext_trig_n_pins,
    ext_trig_p_pins,
    extref_en,
    fpga_10m_n,
    fpga_10m_p,
    fpga_glblclk_clk_n,
    fpga_glblclk_clk_p,
    fpga_mgtrefc1_clk_n,
    fpga_mgtrefc1_clk_p,
    fpga_mgtrefc2_clk_n,
    fpga_mgtrefc2_clk_p,
    fpga_sysref_n,
    fpga_sysref_p,
    gpdi_n,
    gpdi_p,
    gpdo_enable_n,
    gpdo_n,
    gpdo_p,
    gpio,
    gpio_dir,
    gpio_pwr_enable_n,
    gpio_pwr_fault_n,
    iic_scl_io,
    iic_sda_io,
    led_pwr,
    led_rdy,
    led_stat,
    led_user,
    ltm4633,
    ltm4644a,
    ltm4644b,
    pcie_gt_rxn,
    pcie_gt_rxp,
    pcie_gt_txn,
    pcie_gt_txp,
    pcie_perstn,
    pcie_refclk_clk_n,
    pcie_refclk_clk_p,
    pcie_usb3_gt_rxn,
    pcie_usb3_gt_rxp,
    pcie_usb3_gt_txn,
    pcie_usb3_gt_txp,
    pcie_usb3_perstn,
    pcie_usb3_refclk_clk_n,
    pcie_usb3_refclk_clk_p,
    pll_cs_n,
    pll_reset,
    pll_sync,
    pxi_trig0,
    pxi_trig0_dir,
    pxi_trig1,
    pxi_trig1_dir,
    pxie_100m_en,
    pxie_10m_en,
    pxie_atnled_r,
    pxie_ga,
    pxie_stara,
    pxie_starb_n,
    pxie_starb_p,
    pxie_starc_n,
    pxie_starc_p,
    pxie_sync_en,
    rly_cha,
    rly_chb,
    sclk,
    sdio,
    secee_1wire,
    spi_eeprom_io0_io,
    spi_eeprom_io1_io,
    spi_eeprom_sck_io,
    spi_eeprom_ss_io,
    sync_gpio_n,
    sync_gpio_p,
    sync_neg1,
    sync_neg2,
    sync_out,
    sync_out_en,
    trig_out,
    trig_out_en,
    trigdac_cs_n,
    vcxodac_cs_n,
    vsel);
  input Vaux0_v_n;
  input Vaux0_v_p;
  input Vaux1_v_n;
  input Vaux1_v_p;
  input Vaux8_v_n;
  input Vaux8_v_p;
  input Vaux9_v_n;
  input Vaux9_v_p;
  input Vp_Vn_v_n;
  input Vp_Vn_v_p;
  output a_ddr4_act_n;
  output [16:0]a_ddr4_adr;
  output [1:0]a_ddr4_ba;
  output [0:0]a_ddr4_bg;
  output [0:0]a_ddr4_ck_c;
  output [0:0]a_ddr4_ck_t;
  output [0:0]a_ddr4_cke;
  inout [7:0]a_ddr4_dm_n;
  inout [63:0]a_ddr4_dq;
  inout [7:0]a_ddr4_dqs_c;
  inout [7:0]a_ddr4_dqs_t;
  output [0:0]a_ddr4_odt;
  output a_ddr4_reset_n;
  input a_sys_clk_n;
  input a_sys_clk_p;
  output [0:0]adc1_cs_n;
  input [7:0]adc1_d_n;
  input [7:0]adc1_d_p;
  output adc1_pdwn;
  output adc1_sync_n;
  output adc1_sync_p;
  output [0:0]adc2_cs_n;
  input [7:0]adc2_d_n;
  input [7:0]adc2_d_p;
  output adc2_pdwn;
  output adc2_sync_n;
  output adc2_sync_p;
  output adc_sclk;
  inout adc_sdio;
  output b_ddr4_act_n;
  output [16:0]b_ddr4_adr;
  output [1:0]b_ddr4_ba;
  output [0:0]b_ddr4_bg;
  output [0:0]b_ddr4_ck_c;
  output [0:0]b_ddr4_ck_t;
  output [0:0]b_ddr4_cke;
  inout [7:0]b_ddr4_dm_n;
  inout [63:0]b_ddr4_dq;
  inout [7:0]b_ddr4_dqs_c;
  inout [7:0]b_ddr4_dqs_t;
  output [0:0]b_ddr4_odt;
  output b_ddr4_reset_n;
  input b_sys_clk_n;
  input b_sys_clk_p;
  inout cfpga_io0_io;
  inout cfpga_io1_io;
  inout cfpga_io2_io;
  inout cfpga_io3_io;
  output cfpga_r_w_n;
  output cfpga_sck_io;
  output cfpga_ss_io;
  output clkbufout_en_n;
  output [0:0]cpll_cs_n;
  output [0:0]dac_bias_cs_n;
  input db_pwr_gd_n;
  input [15:0]ext_trig_n_pins;
  input [15:0]ext_trig_p_pins;
  output extref_en;
  input fpga_10m_n;
  input fpga_10m_p;
  input [0:0]fpga_glblclk_clk_n;
  input [0:0]fpga_glblclk_clk_p;
  input [0:0]fpga_mgtrefc1_clk_n;
  input [0:0]fpga_mgtrefc1_clk_p;
  input [0:0]fpga_mgtrefc2_clk_n;
  input [0:0]fpga_mgtrefc2_clk_p;
  input fpga_sysref_n;
  input fpga_sysref_p;
  input [3:0]gpdi_n;
  input [3:0]gpdi_p;
  output gpdo_enable_n;
  output [2:0]gpdo_n;
  output [2:0]gpdo_p;
  inout [11:0]gpio;
  output [5:0]gpio_dir;
  output gpio_pwr_enable_n;
  input gpio_pwr_fault_n;
  inout iic_scl_io;
  inout iic_sda_io;
  output led_pwr;
  output led_rdy;
  output led_stat;
  output led_user;
  output ltm4633;
  output ltm4644a;
  output ltm4644b;
  input [7:0]pcie_gt_rxn;
  input [7:0]pcie_gt_rxp;
  output [7:0]pcie_gt_txn;
  output [7:0]pcie_gt_txp;
  input pcie_perstn;
  input [0:0]pcie_refclk_clk_n;
  input [0:0]pcie_refclk_clk_p;
  input [0:0]pcie_usb3_gt_rxn;
  input [0:0]pcie_usb3_gt_rxp;
  output [0:0]pcie_usb3_gt_txn;
  output [0:0]pcie_usb3_gt_txp;
  input pcie_usb3_perstn;
  input [0:0]pcie_usb3_refclk_clk_n;
  input [0:0]pcie_usb3_refclk_clk_p;
  output [0:0]pll_cs_n;
  output pll_reset;
  output pll_sync;
  inout pxi_trig0;
  output pxi_trig0_dir;
  inout pxi_trig1;
  output pxi_trig1_dir;
  output pxie_100m_en;
  output pxie_10m_en;
  input pxie_atnled_r;
  input [4:0]pxie_ga;
  input pxie_stara;
  input [1:0]pxie_starb_n;
  input [1:0]pxie_starb_p;
  output pxie_starc_n;
  output pxie_starc_p;
  output pxie_sync_en;
  output rly_cha;
  output rly_chb;
  output sclk;
  inout sdio;
  inout secee_1wire;
  inout spi_eeprom_io0_io;
  inout spi_eeprom_io1_io;
  inout spi_eeprom_sck_io;
  inout [0:0]spi_eeprom_ss_io;
  input sync_gpio_n;
  input sync_gpio_p;
  output sync_neg1;
  output sync_neg2;
  inout sync_out;
  output sync_out_en;
  inout trig_out;
  output trig_out_en;
  output [0:0]trigdac_cs_n;
  output [0:0]vcxodac_cs_n;
  output [5:0]vsel;

  wire Vaux0_v_n;
  wire Vaux0_v_p;
  wire Vaux1_v_n;
  wire Vaux1_v_p;
  wire Vaux8_v_n;
  wire Vaux8_v_p;
  wire Vaux9_v_n;
  wire Vaux9_v_p;
  wire Vp_Vn_v_n;
  wire Vp_Vn_v_p;
  wire a_ddr4_act_n;
  wire [16:0]a_ddr4_adr;
  wire [1:0]a_ddr4_ba;
  wire [0:0]a_ddr4_bg;
  wire [0:0]a_ddr4_ck_c;
  wire [0:0]a_ddr4_ck_t;
  wire [0:0]a_ddr4_cke;
  wire [7:0]a_ddr4_dm_n;
  wire [63:0]a_ddr4_dq;
  wire [7:0]a_ddr4_dqs_c;
  wire [7:0]a_ddr4_dqs_t;
  wire [0:0]a_ddr4_odt;
  wire a_ddr4_reset_n;
  wire a_sys_clk_n;
  wire a_sys_clk_p;
  wire [0:0]adc1_cs_n;
  wire [7:0]adc1_d_n;
  wire [7:0]adc1_d_p;
  wire adc1_pdwn;
  wire adc1_sync_n;
  wire adc1_sync_p;
  wire [0:0]adc2_cs_n;
  wire [7:0]adc2_d_n;
  wire [7:0]adc2_d_p;
  wire adc2_pdwn;
  wire adc2_sync_n;
  wire adc2_sync_p;
  wire adc_sclk;
  wire adc_sdio;
  wire b_ddr4_act_n;
  wire [16:0]b_ddr4_adr;
  wire [1:0]b_ddr4_ba;
  wire [0:0]b_ddr4_bg;
  wire [0:0]b_ddr4_ck_c;
  wire [0:0]b_ddr4_ck_t;
  wire [0:0]b_ddr4_cke;
  wire [7:0]b_ddr4_dm_n;
  wire [63:0]b_ddr4_dq;
  wire [7:0]b_ddr4_dqs_c;
  wire [7:0]b_ddr4_dqs_t;
  wire [0:0]b_ddr4_odt;
  wire b_ddr4_reset_n;
  wire b_sys_clk_n;
  wire b_sys_clk_p;
  wire cfpga_io0_io;
  wire cfpga_io1_io;
  wire cfpga_io2_io;
  wire cfpga_io3_io;
  wire cfpga_r_w_n;
  wire cfpga_sck_io;
  wire cfpga_ss_io;
  wire clkbufout_en_n;
  wire [0:0]cpll_cs_n;
  wire [0:0]dac_bias_cs_n;
  wire db_pwr_gd_n;
  wire [15:0]ext_trig_n_pins;
  wire [15:0]ext_trig_p_pins;
  wire extref_en;
  wire fpga_10m_n;
  wire fpga_10m_p;
  wire [0:0]fpga_glblclk_clk_n;
  wire [0:0]fpga_glblclk_clk_p;
  wire [0:0]fpga_mgtrefc1_clk_n;
  wire [0:0]fpga_mgtrefc1_clk_p;
  wire [0:0]fpga_mgtrefc2_clk_n;
  wire [0:0]fpga_mgtrefc2_clk_p;
  wire fpga_sysref_n;
  wire fpga_sysref_p;
  wire [3:0]gpdi_n;
  wire [3:0]gpdi_p;
  wire gpdo_enable_n;
  wire [2:0]gpdo_n;
  wire [2:0]gpdo_p;
  wire [11:0]gpio;
  wire [5:0]gpio_dir;
  wire gpio_pwr_enable_n;
  wire gpio_pwr_fault_n;
  wire iic_scl_i;
  wire iic_scl_io;
  wire iic_scl_o;
  wire iic_scl_t;
  wire iic_sda_i;
  wire iic_sda_io;
  wire iic_sda_o;
  wire iic_sda_t;
  wire led_pwr;
  wire led_rdy;
  wire led_stat;
  wire led_user;
  wire ltm4633;
  wire ltm4644a;
  wire ltm4644b;
  wire [7:0]pcie_gt_rxn;
  wire [7:0]pcie_gt_rxp;
  wire [7:0]pcie_gt_txn;
  wire [7:0]pcie_gt_txp;
  wire pcie_perstn;
  wire [0:0]pcie_refclk_clk_n;
  wire [0:0]pcie_refclk_clk_p;
  wire [0:0]pcie_usb3_gt_rxn;
  wire [0:0]pcie_usb3_gt_rxp;
  wire [0:0]pcie_usb3_gt_txn;
  wire [0:0]pcie_usb3_gt_txp;
  wire pcie_usb3_perstn;
  wire [0:0]pcie_usb3_refclk_clk_n;
  wire [0:0]pcie_usb3_refclk_clk_p;
  wire [0:0]pll_cs_n;
  wire pll_reset;
  wire pll_sync;
  wire pxi_trig0;
  wire pxi_trig0_dir;
  wire pxi_trig1;
  wire pxi_trig1_dir;
  wire pxie_100m_en;
  wire pxie_10m_en;
  wire pxie_atnled_r;
  wire [4:0]pxie_ga;
  wire pxie_stara;
  wire [1:0]pxie_starb_n;
  wire [1:0]pxie_starb_p;
  wire pxie_starc_n;
  wire pxie_starc_p;
  wire pxie_sync_en;
  wire rly_cha;
  wire rly_chb;
  wire sclk;
  wire sdio;
  wire secee_1wire;
  wire spi_eeprom_io0_i;
  wire spi_eeprom_io0_io;
  wire spi_eeprom_io0_o;
  wire spi_eeprom_io0_t;
  wire spi_eeprom_io1_i;
  wire spi_eeprom_io1_io;
  wire spi_eeprom_io1_o;
  wire spi_eeprom_io1_t;
  wire spi_eeprom_sck_i;
  wire spi_eeprom_sck_io;
  wire spi_eeprom_sck_o;
  wire spi_eeprom_sck_t;
  wire [0:0]spi_eeprom_ss_i_0;
  wire [0:0]spi_eeprom_ss_io_0;
  wire [0:0]spi_eeprom_ss_o_0;
  wire spi_eeprom_ss_t;
  wire sync_gpio_n;
  wire sync_gpio_p;
  wire sync_neg1;
  wire sync_neg2;
  wire sync_out;
  wire sync_out_en;
  wire trig_out;
  wire trig_out_en;
  wire [0:0]trigdac_cs_n;
  wire [0:0]vcxodac_cs_n;
  wire [5:0]vsel;

  System System_i
       (.IIC_scl_i(iic_scl_i),
        .IIC_scl_o(iic_scl_o),
        .IIC_scl_t(iic_scl_t),
        .IIC_sda_i(iic_sda_i),
        .IIC_sda_o(iic_sda_o),
        .IIC_sda_t(iic_sda_t),
        .SPI_EEPROM_io0_i(spi_eeprom_io0_i),
        .SPI_EEPROM_io0_o(spi_eeprom_io0_o),
        .SPI_EEPROM_io0_t(spi_eeprom_io0_t),
        .SPI_EEPROM_io1_i(spi_eeprom_io1_i),
        .SPI_EEPROM_io1_o(spi_eeprom_io1_o),
        .SPI_EEPROM_io1_t(spi_eeprom_io1_t),
        .SPI_EEPROM_sck_i(spi_eeprom_sck_i),
        .SPI_EEPROM_sck_o(spi_eeprom_sck_o),
        .SPI_EEPROM_sck_t(spi_eeprom_sck_t),
        .SPI_EEPROM_ss_i(spi_eeprom_ss_i_0),
        .SPI_EEPROM_ss_o(spi_eeprom_ss_o_0),
        .SPI_EEPROM_ss_t(spi_eeprom_ss_t),
        .Vaux0_v_n(Vaux0_v_n),
        .Vaux0_v_p(Vaux0_v_p),
        .Vaux1_v_n(Vaux1_v_n),
        .Vaux1_v_p(Vaux1_v_p),
        .Vaux8_v_n(Vaux8_v_n),
        .Vaux8_v_p(Vaux8_v_p),
        .Vaux9_v_n(Vaux9_v_n),
        .Vaux9_v_p(Vaux9_v_p),
        .Vp_Vn_v_n(Vp_Vn_v_n),
        .Vp_Vn_v_p(Vp_Vn_v_p),
        .a_ddr4_act_n(a_ddr4_act_n),
        .a_ddr4_adr(a_ddr4_adr),
        .a_ddr4_ba(a_ddr4_ba),
        .a_ddr4_bg(a_ddr4_bg),
        .a_ddr4_ck_c(a_ddr4_ck_c),
        .a_ddr4_ck_t(a_ddr4_ck_t),
        .a_ddr4_cke(a_ddr4_cke),
        .a_ddr4_dm_n(a_ddr4_dm_n),
        .a_ddr4_dq(a_ddr4_dq),
        .a_ddr4_dqs_c(a_ddr4_dqs_c),
        .a_ddr4_dqs_t(a_ddr4_dqs_t),
        .a_ddr4_odt(a_ddr4_odt),
        .a_ddr4_reset_n(a_ddr4_reset_n),
        .a_sys_clk_n(a_sys_clk_n),
        .a_sys_clk_p(a_sys_clk_p),
        .adc1_cs_n(adc1_cs_n),
        .adc1_d_n(adc1_d_n),
        .adc1_d_p(adc1_d_p),
        .adc1_pdwn(adc1_pdwn),
        .adc1_sync_n(adc1_sync_n),
        .adc1_sync_p(adc1_sync_p),
        .adc2_cs_n(adc2_cs_n),
        .adc2_d_n(adc2_d_n),
        .adc2_d_p(adc2_d_p),
        .adc2_pdwn(adc2_pdwn),
        .adc2_sync_n(adc2_sync_n),
        .adc2_sync_p(adc2_sync_p),
        .adc_sclk(adc_sclk),
        .adc_sdio(adc_sdio),
        .b_ddr4_act_n(b_ddr4_act_n),
        .b_ddr4_adr(b_ddr4_adr),
        .b_ddr4_ba(b_ddr4_ba),
        .b_ddr4_bg(b_ddr4_bg),
        .b_ddr4_ck_c(b_ddr4_ck_c),
        .b_ddr4_ck_t(b_ddr4_ck_t),
        .b_ddr4_cke(b_ddr4_cke),
        .b_ddr4_dm_n(b_ddr4_dm_n),
        .b_ddr4_dq(b_ddr4_dq),
        .b_ddr4_dqs_c(b_ddr4_dqs_c),
        .b_ddr4_dqs_t(b_ddr4_dqs_t),
        .b_ddr4_odt(b_ddr4_odt),
        .b_ddr4_reset_n(b_ddr4_reset_n),
        .b_sys_clk_n(b_sys_clk_n),
        .b_sys_clk_p(b_sys_clk_p),
        .cfpga_io0_io(cfpga_io0_io),
        .cfpga_io1_io(cfpga_io1_io),
        .cfpga_io2_io(cfpga_io2_io),
        .cfpga_io3_io(cfpga_io3_io),
        .cfpga_r_w_n(cfpga_r_w_n),
        .cfpga_sck_io(cfpga_sck_io),
        .cfpga_ss_io(cfpga_ss_io),
        .clkbufout_en_n(clkbufout_en_n),
        .cpll_cs_n(cpll_cs_n),
        .dac_bias_cs_n(dac_bias_cs_n),
        .db_pwr_gd_n(db_pwr_gd_n),
        .ext_trig_n_pins(ext_trig_n_pins),
        .ext_trig_p_pins(ext_trig_p_pins),
        .extref_en(extref_en),
        .fpga_10m_n(fpga_10m_n),
        .fpga_10m_p(fpga_10m_p),
        .fpga_glblclk_clk_n(fpga_glblclk_clk_n),
        .fpga_glblclk_clk_p(fpga_glblclk_clk_p),
        .fpga_mgtrefc1_clk_n(fpga_mgtrefc1_clk_n),
        .fpga_mgtrefc1_clk_p(fpga_mgtrefc1_clk_p),
        .fpga_mgtrefc2_clk_n(fpga_mgtrefc2_clk_n),
        .fpga_mgtrefc2_clk_p(fpga_mgtrefc2_clk_p),
        .fpga_sysref_n(fpga_sysref_n),
        .fpga_sysref_p(fpga_sysref_p),
        .gpdi_n(gpdi_n),
        .gpdi_p(gpdi_p),
        .gpdo_enable_n(gpdo_enable_n),
        .gpdo_n(gpdo_n),
        .gpdo_p(gpdo_p),
        .gpio(gpio),
        .gpio_dir(gpio_dir),
        .gpio_pwr_enable_n(gpio_pwr_enable_n),
        .gpio_pwr_fault_n(gpio_pwr_fault_n),
        .led_pwr(led_pwr),
        .led_rdy(led_rdy),
        .led_stat(led_stat),
        .led_user(led_user),
        .ltm4633(ltm4633),
        .ltm4644a(ltm4644a),
        .ltm4644b(ltm4644b),
        .pcie_gt_rxn(pcie_gt_rxn),
        .pcie_gt_rxp(pcie_gt_rxp),
        .pcie_gt_txn(pcie_gt_txn),
        .pcie_gt_txp(pcie_gt_txp),
        .pcie_perstn(pcie_perstn),
        .pcie_refclk_clk_n(pcie_refclk_clk_n),
        .pcie_refclk_clk_p(pcie_refclk_clk_p),
        .pcie_usb3_gt_rxn(pcie_usb3_gt_rxn),
        .pcie_usb3_gt_rxp(pcie_usb3_gt_rxp),
        .pcie_usb3_gt_txn(pcie_usb3_gt_txn),
        .pcie_usb3_gt_txp(pcie_usb3_gt_txp),
        .pcie_usb3_perstn(pcie_usb3_perstn),
        .pcie_usb3_refclk_clk_n(pcie_usb3_refclk_clk_n),
        .pcie_usb3_refclk_clk_p(pcie_usb3_refclk_clk_p),
        .pll_cs_n(pll_cs_n),
        .pll_reset(pll_reset),
        .pll_sync(pll_sync),
        .pxi_trig0(pxi_trig0),
        .pxi_trig0_dir(pxi_trig0_dir),
        .pxi_trig1(pxi_trig1),
        .pxi_trig1_dir(pxi_trig1_dir),
        .pxie_100m_en(pxie_100m_en),
        .pxie_10m_en(pxie_10m_en),
        .pxie_atnled_r(pxie_atnled_r),
        .pxie_ga(pxie_ga),
        .pxie_stara(pxie_stara),
        .pxie_starb_n(pxie_starb_n),
        .pxie_starb_p(pxie_starb_p),
        .pxie_starc_n(pxie_starc_n),
        .pxie_starc_p(pxie_starc_p),
        .pxie_sync_en(pxie_sync_en),
        .rly_cha(rly_cha),
        .rly_chb(rly_chb),
        .sclk(sclk),
        .sdio(sdio),
        .secee_1wire(secee_1wire),
        .sync_gpio_n(sync_gpio_n),
        .sync_gpio_p(sync_gpio_p),
        .sync_neg1(sync_neg1),
        .sync_neg2(sync_neg2),
        .sync_out(sync_out),
        .sync_out_en(sync_out_en),
        .trig_out(trig_out),
        .trig_out_en(trig_out_en),
        .trigdac_cs_n(trigdac_cs_n),
        .vcxodac_cs_n(vcxodac_cs_n),
        .vsel(vsel));
  IOBUF iic_scl_iobuf
       (.I(iic_scl_o),
        .IO(iic_scl_io),
        .O(iic_scl_i),
        .T(iic_scl_t));
  IOBUF iic_sda_iobuf
       (.I(iic_sda_o),
        .IO(iic_sda_io),
        .O(iic_sda_i),
        .T(iic_sda_t));
  IOBUF spi_eeprom_io0_iobuf
       (.I(spi_eeprom_io0_o),
        .IO(spi_eeprom_io0_io),
        .O(spi_eeprom_io0_i),
        .T(spi_eeprom_io0_t));
  IOBUF spi_eeprom_io1_iobuf
       (.I(spi_eeprom_io1_o),
        .IO(spi_eeprom_io1_io),
        .O(spi_eeprom_io1_i),
        .T(spi_eeprom_io1_t));
  IOBUF spi_eeprom_sck_iobuf
       (.I(spi_eeprom_sck_o),
        .IO(spi_eeprom_sck_io),
        .O(spi_eeprom_sck_i),
        .T(spi_eeprom_sck_t));
  IOBUF spi_eeprom_ss_iobuf_0
       (.I(spi_eeprom_ss_o_0),
        .IO(spi_eeprom_ss_io[0]),
        .O(spi_eeprom_ss_i_0),
        .T(spi_eeprom_ss_t));
endmodule
