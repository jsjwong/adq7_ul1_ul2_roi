/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: Saturated rounding module. The rounding mode is "round to
 *              nearest" with the tie breaking rule specified by the TIE
 *              input parameter (a string).
 *                - "AWAYFROMZERO": round(-0.5) = -1, round(0.5) = 1
 *                - "TOEVEN"      : round(-0.5) = 0,  round(0.5) = 0,
 *                                  round(1.5)  = 2,  round(2.5) = 2
 *              The output is truncated to OUTPUT_WIDTH bits and saturated
 *              if the value is outside the range.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_saturated_rounding #(
   TIE = "AWAYFROMZERO",
   FRAC_WIDTH = 0,
   INPUT_WIDTH = 0,
   OUTPUT_WIDTH = 0,
   PIPELINE_CHECKER = 0
)(
   input wire clk_i,
   input wire rst_i,
   input wire [INPUT_WIDTH-1:0] data_i,
   input wire data_valid_i,

   output wire [OUTPUT_WIDTH-1:0] data_o,
   output wire data_valid_o
);
   /* Parameter validation */
   if (PIPELINE_CHECKER != 2)
      always ERROR_SATURATED_ROUNDING_PIPELINE_CHECKER_WRONG_VALUE();
   if (FRAC_WIDTH < 2)
      always ERROR_SATURATED_ROUNDING_FRAC_WIDTH_IS_LESS_THAN_TWO();
   if (OUTPUT_WIDTH < 1)
      always ERROR_SATURATED_ROUNDING_OUTPUT_WIDTH_IS_LESS_THAN_ONE();

   /* Local parameters, wires & registers */
   localparam GUARD_BIT = 1;

   /* First stage after rounding, add guard bit for addition with the rounder
      value and add one since we need to keep the first fractional bit. */
   localparam ROUNDER_WIDTH = INPUT_WIDTH - FRAC_WIDTH + GUARD_BIT + 1;

   /* Remove the remaining fractional bit. */
   localparam TRUNC_WIDTH = ROUNDER_WIDTH - 1;

   reg [ROUNDER_WIDTH-1:0] data_rounded = {(ROUNDER_WIDTH){1'b0}};
   reg data_valid_rounded;

   wire [ROUNDER_WIDTH-1:0] rounder;
   wire rounder_select;

   wire [TRUNC_WIDTH-1:0] data_truncated;

   reg [OUTPUT_WIDTH-1:0] data_saturated = {(OUTPUT_WIDTH){1'b0}};
   reg data_valid_saturated;

   wire is_positive;
   wire is_negative;

   /* We describe the rule to use -0.5 as the rounder value. For every other
      case 0.5 is used. */
   assign rounder_select =
      /* If sign bit is set */
      data_i[INPUT_WIDTH-1]
      /* Bit w/ value 0.5 is set */
      & data_i[FRAC_WIDTH-1]
      /* None of the lesser valued fractional are set */
      & ~(|data_i[FRAC_WIDTH-2:0]);

   assign rounder = rounder_select ? {(ROUNDER_WIDTH){1'b1}}
                                   : {{(ROUNDER_WIDTH-1){1'b0}}, 1'b1};

   /* Truncate to one fractional bit and add the rounding value */
   always @(posedge clk_i) begin
      if (rst_i)
         data_rounded <= {(ROUNDER_WIDTH){1'b0}};
      else
         data_rounded <= $signed(data_i[INPUT_WIDTH-1:FRAC_WIDTH-1])
                         + $signed(rounder);

      data_valid_rounded <= data_valid_i;
   end

   assign data_truncated = data_rounded[1 +: TRUNC_WIDTH];

   /* Saturate and truncate the filter output further. */

   assign is_negative = (data_truncated[TRUNC_WIDTH-1:OUTPUT_WIDTH-1]
                        == {(TRUNC_WIDTH-OUTPUT_WIDTH+1){1'b1}});
   assign is_positive = (data_truncated[TRUNC_WIDTH-1:OUTPUT_WIDTH-1]
                        == {(TRUNC_WIDTH-OUTPUT_WIDTH+1){1'b0}});

   always @(posedge clk_i) begin
      if (rst_i) begin
         data_saturated <= {(OUTPUT_WIDTH){1'b0}};
      end else begin
         if (is_positive | is_negative) begin
            /* Just truncate the result */
            data_saturated <= data_truncated[0 +: OUTPUT_WIDTH];
         end else begin
            /* Saturate... */
            if (data_truncated[TRUNC_WIDTH-1])
               /* ... to MIN code */
               data_saturated <= {1'b1, {(OUTPUT_WIDTH-1){1'b0}}};
            else
               /* ... to MAX code */
               data_saturated <= {1'b0, {(OUTPUT_WIDTH-1){1'b1}}};
         end
      end
      data_valid_saturated <= data_valid_rounded;
   end

   assign data_o       = data_saturated;
   assign data_valid_o = data_valid_saturated;

endmodule

`default_nettype wire
