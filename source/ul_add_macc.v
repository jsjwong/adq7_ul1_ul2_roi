/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: Pre-add, multiply and accumulate module for FIR filter
 *              design. This is directly mapped onto one single DSP48E2
 *              element.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_add_macc #(
   AWIDTH = 0,
   BWIDTH = 0,
   CWIDTH = 0,
   DWIDTH = 0,
   PWIDTH = 0,
   CDELAY = 0,
   CONFIG = "DEFAULT",
   MODE   = "ALWAYS_ENABLED",
   SETUP_CHECKER = 0,
   PIPELINE_CHECKER = 0
)(
   input wire clk_i,
   input wire rst_i,

   input wire [AWIDTH-1:0] a_i,
   input wire [BWIDTH-1:0] b_i,
   input wire [CWIDTH-1:0] c_i,
   input wire [DWIDTH-1:0] d_i,
   input wire valid_i,

   output wire [PWIDTH-1:0] p_o,
   output wire valid_o
);
   `include "ul_clog2.vh"

   localparam DELAY_DSP = 4;
   localparam SETUP_DSP_CLOG2 = clog2(DELAY_DSP);

   /* Actual widths of primitive connections */
   localparam AWIDTH_ACTUAL = 30;
   localparam BWIDTH_ACTUAL = 18;
   localparam CWIDTH_ACTUAL = 48;
   localparam DWIDTH_ACTUAL = 27;
   localparam PWIDTH_ACTUAL = 48;

   /* Parameter validation */
   if ((AWIDTH > AWIDTH_ACTUAL) || (AWIDTH == 0))
      always ERROR_ADD_MACC_AWIDTH();
   if ((BWIDTH > BWIDTH_ACTUAL) || (BWIDTH == 0))
      always ERROR_ADD_MACC_BWIDTH();
   if ((CWIDTH > CWIDTH_ACTUAL) || (CWIDTH == 0))
      always ERROR_ADD_MACC_CWIDTH();
   if ((DWIDTH > DWIDTH_ACTUAL) || (DWIDTH == 0))
      always ERROR_ADD_MACC_DWIDTH();
   if ((PWIDTH > PWIDTH_ACTUAL) || (PWIDTH == 0))
      always ERROR_ADD_MACC_PWIDTH();

   if ((MODE == "VALID") && (SETUP_CHECKER != DELAY_DSP))
      always ERROR_ADD_MACC_SETUP_CHECKER();
   if ((MODE == "VALID") && (PIPELINE_CHECKER != 0))
      always ERROR_ADD_MACC_PIPELINE_CHECKER();

   if ((MODE == "ALWAYS_ENABLED") && (SETUP_CHECKER != 0))
      always ERROR_ADD_MACC_SETUP_CHECKER();
   if ((MODE == "ALWAYS_ENABLED") && (PIPELINE_CHECKER != DELAY_DSP))
      always ERROR_ADD_MACC_PIPELINE_CHECKER();

   if ((CONFIG != "DEFAULT") && (PWIDTH != CWIDTH))
      always ERROR_CASCADE_MODE_REQUIRES_PWIDTH_EQ_CWIDTH();
   if ((CONFIG != "DEFAULT") && (PWIDTH != PWIDTH_ACTUAL))
      always ERROR_CASCADE_MODE_REQUIRES_PWIDTH_48();

   /* Local parameters, wires & registers */
   wire ce_in;
   wire rst_in;

   wire [CWIDTH-1:0] c_d;

   wire [AWIDTH_ACTUAL-1:0] a_in;
   wire [BWIDTH_ACTUAL-1:0] b_in;
   wire [CWIDTH_ACTUAL-1:0] c_in;
   wire [DWIDTH_ACTUAL-1:0] d_in;
   wire [PWIDTH_ACTUAL-1:0] pc_in;
   wire [PWIDTH_ACTUAL-1:0] p_out;
   wire [PWIDTH_ACTUAL-1:0] pc_out;
   wire [8:0] op_mode;

   if (CDELAY > 0) begin
      /* Extra input pipeline stages for the C signal */
      reg [CWIDTH-1:0] c_r [CDELAY-1:0];
      integer i;
      always @(posedge clk_i) begin
         if (rst_i) begin
            for (i = 0; i < CDELAY; i = i + 1) begin
               c_r[i] <= {PWIDTH{1'b0}};
            end
         end else begin
            if (ce_in) begin
               for (i = 0; i < CDELAY-1; i = i + 1) begin
                  c_r[i] <= c_r[i+1];
               end
               c_r[CDELAY-1] <= c_i;
            end
         end
      end
      assign c_d = c_r[0];
   end else begin
      assign c_d = c_i;
   end

   /* Sign extension up to the actual port widths. */
   assign a_in = $signed(a_i);
   assign b_in = $signed(b_i);
   assign d_in = $signed(d_i);

   if (MODE == "ALWAYS_ENABLED")
      assign ce_in = 1'b1;
   else if (MODE == "VALID")
      assign ce_in = valid_i;
   else
      always ERROR_ADD_MACC_UNSUPPORTED_MODE();

   assign rst_in = rst_i;

   /* DSP48E2 connections */
   if (CONFIG == "FIRST") begin
      /* First DSP element in cascade */
      assign op_mode = 9'b000000101;
      assign c_in    = {(CWIDTH_ACTUAL){1'bX}};
      assign pc_in   = {(PWIDTH_ACTUAL){1'bX}};
      assign p_o     = pc_out[0 +: PWIDTH];
   end else if (CONFIG == "LAST") begin
      /* Last DSP element in cascade. */
      assign op_mode = 9'b000010101;
      assign c_in    = {(CWIDTH_ACTUAL){1'bX}};
      assign pc_in   = $signed(c_d);
      assign p_o     = p_out[0 +: PWIDTH];
   end else if (CONFIG == "CASCADE") begin
      /* Any other DSP element in the cascade chain. */
      assign op_mode = 9'b000010101;
      assign c_in    = {(CWIDTH_ACTUAL){1'bX}};
      assign pc_in   = $signed(c_d);
      assign p_o     = pc_out[0 +: PWIDTH];
   end else begin
      /* Not using any cascade path. */
      assign op_mode = 9'b000110101;
      assign c_in    = $signed(c_d);
      assign pc_in   = {(PWIDTH_ACTUAL){1'bX}};
      assign p_o     = p_out[0 +: PWIDTH];
   end

   /* DSP48E2 instantiation */
   `include "ul_dsp_primitive.vh"

   /* Data valid handling. */
   if (MODE == "ALWAYS_ENABLED") begin
      reg [DELAY_DSP-1:0] valid_sr = 'd0;

      always @(posedge clk_i) begin
         valid_sr <= {valid_sr[DELAY_DSP-2:0], valid_i};
      end

      assign valid_o = valid_sr[DELAY_DSP-1];
   end else begin
      reg [SETUP_DSP_CLOG2-1:0] valid_ctr = {(SETUP_DSP_CLOG2){1'b0}};
      wire valid_ctr_max_reached;

      always @(posedge clk_i) begin
         if (rst_i) begin
            valid_ctr <= {(SETUP_DSP_CLOG2){1'b0}};
         end else begin
            if (valid_ctr_max_reached & ~ce_in)
               valid_ctr <= valid_ctr - {{(SETUP_DSP_CLOG2-1){1'b0}}, 1'b1};
            else if (~valid_ctr_max_reached & ce_in)
               valid_ctr <= valid_ctr + {{(SETUP_DSP_CLOG2-1){1'b0}}, 1'b1};
         end
      end

      assign valid_ctr_max_reached = (valid_ctr == DELAY_DSP);
      assign valid_o = valid_ctr_max_reached;
   end


endmodule

`default_nettype wire
