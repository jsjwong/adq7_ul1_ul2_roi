/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description   : User register file
 * Documentation :
 *
 */

module ul1_regfile #(
   parameter ADDR_WIDTH = 0
)(
   input wire clk,
   input wire rst_i,

   input wire [ADDR_WIDTH-1:0] addr_i,

   input wire [31:0] wr_data_i,
   input wire wr_i,
   output reg wr_ack_o,

   output reg [31:0] rd_data_o,
   input wire rd_i,
   output reg rd_ack_o,

   input wire [31:0] reg_0x04_i,
   input wire [31:0] reg_0x05_i,
   input wire [31:0] reg_0x06_i,
   input wire [31:0] reg_0x10_i,
   input wire [31:0] reg_0x11_i,
   input wire [31:0] reg_0x12_i,
   input wire [31:0] reg_0x13_i,

   output wire [31:0] reg_0x04_o,
   output wire [31:0] reg_0x05_o,
   output wire [31:0] reg_0x06_o,
   output wire [31:0] reg_0x10_o,
   output wire [31:0] reg_0x11_o,
   output wire [31:0] reg_0x12_o,
   output wire [31:0] reg_0x13_o
);

   reg [31:0] regfile_out [6:0];
   wire [31:0] regfile_in [6:0];

   assign reg_0x04_o = regfile_out[0];
   assign reg_0x05_o = regfile_out[1];
   assign reg_0x06_o = regfile_out[2];
   assign reg_0x10_o = regfile_out[3];
   assign reg_0x11_o = regfile_out[4];
   assign reg_0x12_o = regfile_out[5];
   assign reg_0x13_o = regfile_out[6];

   assign regfile_in[0] = reg_0x04_i;
   assign regfile_in[1] = reg_0x05_i;
   assign regfile_in[2] = reg_0x06_i;
   assign regfile_in[3] = reg_0x10_i;
   assign regfile_in[4] = reg_0x11_i;
   assign regfile_in[5] = reg_0x12_i;
   assign regfile_in[6] = reg_0x13_i;

   always @(posedge clk) begin
      wr_ack_o <= wr_i;
      rd_ack_o <= rd_i;
   end

   always @(posedge clk) begin
      if (rd_i) begin
         case (addr_i)
            14'h04: rd_data_o <= regfile_in[0];
            14'h05: rd_data_o <= regfile_in[1];
            14'h06: rd_data_o <= regfile_in[2];
            14'h10: rd_data_o <= regfile_in[3];
            14'h11: rd_data_o <= regfile_in[4];
            14'h12: rd_data_o <= regfile_in[5];
            14'h13: rd_data_o <= regfile_in[6];
         endcase
      end
   end

   integer i;
   always @(posedge clk) begin
      if (rst_i) begin
        for (i = 0; i < 7; i = i + 1) begin
           regfile_out[i] <= 0;
        end
      end else begin
         if (wr_i) begin
            case (addr_i)
               14'h04: regfile_out[0] <= wr_data_i;
               14'h05: regfile_out[1] <= wr_data_i;
               14'h06: regfile_out[2] <= wr_data_i;
               14'h10: regfile_out[3] <= wr_data_i;
               14'h11: regfile_out[4] <= wr_data_i;
               14'h12: regfile_out[5] <= wr_data_i;
               14'h13: regfile_out[6] <= wr_data_i;
            endcase
         end
      end
   end
endmodule
