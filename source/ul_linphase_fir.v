/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: Linear-phase FIR filter using a parallel structure.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_linphase_fir #(
   DATAWIDTH_BITS   = 0,
   PARALLEL_SAMPLES = 0,
   COEFF_WIDTH      = 0,
   ORDER            = 0,
   COEFF_FRAC_WIDTH = 0,
   SETUP_CHECKER    = 0,
   PIPELINE_CHECKER = 0
)(
   input wire clk_i,
   input wire rst_i,

   /* Input data */
   input wire [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_i,
   input wire data_valid_i,

   /* Filter coefficients */
   input wire [COEFF_WIDTH*(ORDER/2+1)-1:0] coeffs_i,

   /* Output data, subjected to saturated rounding */
   output wire [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_o,
   output wire data_valid_o
);
   /* Local parameters */
   localparam FIRLEN = ORDER + 1;
   localparam DWBITS = DATAWIDTH_BITS;
   localparam NOFPS  = PARALLEL_SAMPLES;
   localparam NOFMEM = (ORDER + NOFPS - 1) / NOFPS;

   /* The barrel shifter will output its result in the next cycle after having
      observed SETUP_BARREL_SHIFTER cycles. */
   localparam SETUP_BARREL_SHIFTER = 2 + ORDER / (2 * NOFPS);
   localparam PIPELINE_INPUT_STAGE = 1;

   /* Subtract the pipeline delay added by the input stage. */
   localparam PIPELINE_CHECKER_UNIT = PIPELINE_CHECKER - PIPELINE_INPUT_STAGE;

   /* Parameter validation */
   if (DATAWIDTH_BITS != 16)
      always ERROR_LINPHASE_FIR_DATAWIDTH_BITS();
   if (PARALLEL_SAMPLES == 0)
      always ERROR_LINPHASE_FIR_PARALLEL_SAMPLES_IS_ZERO();
   if (NOFMEM < 1)
      always ERROR_LINPHASE_FIR_NOFMEM_CYCLES_LESS_THAN_ONE();

   /* Local wires & registers */
   genvar k;
   genvar j;

   wire [DWBITS-1:0] data_from_filter_s [NOFPS-1:0];
   wire [NOFPS-1:0] data_valid_from_filter_s;

   wire [DWBITS*NOFPS-1:0] data_from_filter;
   wire data_valid_from_filter;

   (* keep = "true" *) reg rst_internal;

   /* Reset replication */
   always @(posedge clk_i) begin
      rst_internal <= rst_i;
   end

   /* Generate linear phase FIR units. */
   generate
      for (k = 0; k < NOFPS; k = k + 1) begin
         reg [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_i_d;
         reg [COEFF_WIDTH*(ORDER/2+1)-1:0] coeffs_i_d;
         reg data_valid_i_d;
         reg [NOFMEM*NOFPS*DWBITS-1:0] data_mem = 'd0;

         wire [(NOFMEM+1)*NOFPS*DWBITS-1:0] data_in;
         wire [DWBITS-1:0] data_in_s [(NOFMEM+1)*NOFPS-1:0];
         wire [DWBITS*FIRLEN-1:0] data_to_fir;

         (* keep = "true" *) reg rst_fir_unit;

         /* Reset replication */
         always @(posedge clk_i) begin
            rst_fir_unit <= rst_i;
         end

         /* Input stage */
         always @(posedge clk_i) begin
            data_i_d       <= data_i;
            data_valid_i_d <= data_valid_i;
            coeffs_i_d     <= coeffs_i;
         end

         /* Memory for samples earlier in time. */
         if (NOFMEM > 1) begin
            always @(posedge clk_i) begin
               if (data_valid_i_d)
                  data_mem <= {
                     data_i_d,
                     data_mem[NOFPS*DWBITS +: (NOFMEM-1)*NOFPS*DWBITS]
                  };
            end
         end else begin
            always @(posedge clk_i) begin
               if (data_valid_i_d)
                  data_mem <= data_i_d;
            end
         end

         /* Current cycle and memory cycles in a vector. */
         assign data_in = {data_i_d, data_mem};

         /* Split data for easier handling */
         for (j = 0; j < NOFPS*(NOFMEM+1); j = j + 1) begin
            assign data_in_s[j] = data_in[j*DWBITS +: DWBITS];
         end

         /* Slice filter data from input data vector. */
         for (j = 0; j < FIRLEN; j = j + 1) begin
            assign data_to_fir[j*DWBITS +: DWBITS] =
               data_in_s[NOFMEM*NOFPS-FIRLEN+1+k+j];
         end

         ul_linphase_fir_unit #(
            .DATAWIDTH_BITS   (DWBITS),
            .COEFF_WIDTH      (COEFF_WIDTH),
            .ORDER            (ORDER),
            .COEFF_FRAC_WIDTH (COEFF_FRAC_WIDTH),
            .PIPELINE_CHECKER (PIPELINE_CHECKER_UNIT)
         ) ul_linphase_fir_unit_inst (
            .clk_i        (clk_i),
            .rst_i        (1'b0),

            .data_i       (data_to_fir),
            .data_valid_i (data_valid_i_d),
            .coeffs_i     (coeffs_i_d),
            .data_o       (data_from_filter_s[k]),
            .data_valid_o (data_valid_from_filter_s[k])
         );

         assign data_from_filter[k*DWBITS +: DWBITS] = data_from_filter_s[k];

`ifdef XILINX_SIMULATOR
            wire [DWBITS-1:0] data_mem_s [NOFMEM*NOFPS-1:0];
            wire [DWBITS-1:0] data_i_d_s [NOFPS-1:0];
            for (j = 0; j < NOFMEM*NOFPS; j = j + 1) begin
               assign data_mem_s[j] = data_mem[j*DWBITS +: DWBITS];
            end
            for (j = 0; j < NOFPS; j = j + 1) begin
               assign data_i_d_s[j] = data_i_d[j*DWBITS +: DWBITS];
            end
`endif
      end
   endgenerate

   assign data_valid_from_filter = data_valid_from_filter_s[0];

   /* Barrel shifter to compensate for the filter group delay. */
   ul_linphase_fir_bs #(
      .DATAWIDTH_BITS   (DWBITS),
      .PARALLEL_SAMPLES (NOFPS),
      .ORDER            (ORDER),
      .SETUP_CHECKER    (SETUP_BARREL_SHIFTER)
   ) ul_linphase_fir_bs_inst (
      .clk_i        (clk_i),
      .rst_i        (1'b0),
      .data_i       (data_from_filter),
      .data_valid_i (data_valid_from_filter),
      .data_o       (data_o),
      .data_valid_o (data_valid_o)
   );

`ifdef XILINX_SIMULATOR
   wire [COEFF_WIDTH-1:0] coeffs_i_s [ORDER/2:0];
   generate
      for (k = 0; k < ORDER/2+1; k = k + 1) begin
         assign coeffs_i_s[k] = coeffs_i[k*COEFF_WIDTH +: COEFF_WIDTH];
      end
   endgenerate
`endif

endmodule

`default_nettype wire
