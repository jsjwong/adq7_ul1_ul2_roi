function integer GetBitWidth; // This returns the minimum bit width needed to represents a binary number b.
//This is NOT equivalent to clog2(b), i.e. Ceil(Log2(b)), it is instead clog2(b+1).
//Note that the clog2(b) implementation in ul_clog2.vh as of r53589 by SP Devices/Teledyne is incorrect! They have in fact implemented clog2(b+1).
   input [63:0] b; //up to 64 bit number
   begin
      for (GetBitWidth = 0; b > 0; GetBitWidth = GetBitWidth + 1) begin
         b = b >> 1;
      end
   end
endfunction
