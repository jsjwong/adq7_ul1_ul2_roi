/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description   : Bus extraction macros, reduced rate
 * Documentation :
 *
 */

/* Must be defined by user before including this file
localparam           BUS_PIPELINE = 1;

wire                      s_axis_aclk;
wire [RR_DATA_BUS_WIDTH-1:0] s_axis_tdata
wire                      s_axis_tvalid;
reg [RR_DATA_BUS_WIDTH-1:0]  m_axis_tdata
reg                       m_axis_tvalid
*/


// RR Bus format:
// DATA   = $SPD_PARALLEL_SAMPLES * $SPD_DATAWIDTH_BITS
// CHT    = $RR_SPD_NUM_CH_TRIG_BITS + $RR_SPD_NUM_TRIG_ADDBITS  + $RR_SPD_NUM_TRIG_DATARD_ADDFRACBITS + 2*$RR_CH_TRIG_BITVECT_WIDTH
// RECORD = $RR_SPD_DATABUSRD_RECORDBITS_WIDTH
// VALID  = $RR_SPD_DATABUSRD_DVALID
// AUX    = $RR_SPD_NUM_AUX_TRIG_BITS + $RR_SPD_NUM_TRIG_ADDBITS + $RR_SPD_NUM_TRIG_DATARD_ADDFRACBITS
// TS     = $RR_SPD_TIMESTAMP_WIDTH_BITS
// OR     = OVER RANGE
// UI     = USer ID
// GP     = General purpose bits
// INH    = Trigger inhibit
// TSSYNC = Timestamp sync (waiting for)
// GATECNT = Gate count

// NOTE: VALID should alwas be last bit!
// BUS    = $SPD_PROCESSING_CHANNELS_FULL * {VALID, TSSYNC, INH, TS, GP, UI, OR, CNT, RECORD, CHT, DATA}, GATECNT, AUX

`include "bus_splitter_rr_param.vh"

// Output
`ifndef DISABLE_OUTPUT_RR
wire [RR_DATA_BUS_WIDTH:0] user_bus_o_default;
reg  [RR_DATA_BUS_WIDTH:0] user_bus_o;

generate
   reg [RR_DATA_BUS_WIDTH:0] bus_pipeline[BUS_PIPELINE-1:0];

   if (BUS_PIPELINE > 0)
     begin
        always@(posedge s_axis_aclk)
          begin : bus_pipeline_inst
             integer i;
             bus_pipeline[0][RR_DATA_BUS_WIDTH]      <= s_axis_tvalid;
             bus_pipeline[0][RR_DATA_BUS_WIDTH-1:0]  <= s_axis_tdata;
             if (BUS_PIPELINE > 1)
               for (i=0; i<BUS_PIPELINE-1; i=i+1)
                 bus_pipeline[i+1]   <= bus_pipeline[i];

          end
     end
   else
     ; //ERROR

   assign user_bus_o_default = bus_pipeline[BUS_PIPELINE-1];
endgenerate

task init_bus_output;
   begin
      user_bus_o = user_bus_o_default;
   end
endtask

task finish_bus_output;
   begin
      m_axis_tdata  = user_bus_o[RR_DATA_BUS_WIDTH-1:0];
      m_axis_tvalid = user_bus_o[RR_DATA_BUS_WIDTH];
   end
endtask

task insert_timestamp;
   input [RR_SPD_TIMESTAMP_WIDTH_BITS-1:0] timestamp;
   input integer ch;
   user_bus_o[RR_CH_FIRST_BIT + ch*RR_CH_WIDTH + RR_CH_POS_TS +: RR_SPD_TIMESTAMP_WIDTH_BITS] = timestamp;
endtask

task insert_trig_inhibit;
   input [RR_SPD_TRIGGER_INHIBIT_BITS-1:0] trig_inhibit;
   input integer ch;
   user_bus_o[RR_CH_FIRST_BIT + ch*RR_CH_WIDTH + RR_CH_POS_TRIGINH +: RR_SPD_TRIGGER_INHIBIT_BITS] = trig_inhibit;
endtask

task insert_timestampsync;
   input [RR_SPD_TSSYNC_BITS-1:0] tssync;
   input integer ch;
   user_bus_o[RR_CH_FIRST_BIT + ch*RR_CH_WIDTH + RR_CH_POS_TSSYNC +: RR_SPD_TSSYNC_BITS] = tssync;
endtask

task insert_aux_trig;
   input [RR_AUX_TRIG_VECTOR_WIDTH-1:0] vector;
   user_bus_o[RR_POS_AUX_TRIG +: RR_AUX_TRIG_VECTOR_WIDTH] = vector;
endtask

// Aux trigger fields
task insert_aux_trig_rnum;
  input [RR_CH_TRIG_NUM_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_RNUM_POS +: RR_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_aux_trig_rrising;
  input [RR_CH_TRIG_RISING_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_RRISING_POS  +: RR_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_aux_trig_revent;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_REVENT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_aux_trig_tnum;
  input [RR_CH_TRIG_NUM_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_TNUM_POS +: RR_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_aux_trig_trising;
  input [RR_CH_TRIG_RISING_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_TRISING_POS  +: RR_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_aux_trig_tevent;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_TEVENT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_aux_trig_revent_pt;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_REVENT_PT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_aux_trig_extended_precision;
  input [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] vector;
  user_bus_o[RR_POS_AUX_TRIG + RR_CH_TRIG_EXTPREC_POS  +: RR_CH_TRIG_EXT_PREC_WIDTH] = vector;
endtask


// Data valid
task insert_data_valid;
   input valid;
   input integer ch;
   user_bus_o[RR_CH_FIRST_BIT + ch*RR_CH_WIDTH + RR_CH_WIDTH-1 +: 1] = valid;
endtask

task insert_ch_all;
   input [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] data;
   input integer ch;
   user_bus_o[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH +: SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS] = data;
endtask


task insert_ch_trig_vector;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_CHTRIG +: RR_CH_TRIG_VECTOR_1_WIDTH] = data;
endtask

// Channel trig fields

task insert_ch_trig_rnum;
  input [RR_CH_TRIG_NUM_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_RNUM_POS +: RR_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_ch_trig_rrising;
  input [RR_CH_TRIG_RISING_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_RRISING_POS  +: RR_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_ch_trig_revent;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_REVENT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_ch_trig_tnum;
  input [RR_CH_TRIG_NUM_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_TNUM_POS +: RR_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_ch_trig_trising;
  input [RR_CH_TRIG_RISING_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_TRISING_POS  +: RR_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_ch_trig_tevent;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_TEVENT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_ch_trig_revent_pt;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_REVENT_PT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_ch_trig_extended_precision;
  input [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_EXTPREC_POS  +: RR_CH_TRIG_EXT_PREC_WIDTH] = vector;
endtask

task insert_ch_trig_tevent_bitvect;
  input [RR_CH_TRIG_BITVECT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_TEVENT_BITVECT_POS  +: RR_CH_TRIG_BITVECT_WIDTH] = vector;
endtask

task insert_ch_trig_revent_bitvect;
  input [RR_CH_TRIG_BITVECT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + RR_CH_WIDTH*ch + RR_CH_POS_CHTRIG + RR_CH_TRIG_REVENT_BITVECT_POS  +: RR_CH_TRIG_BITVECT_WIDTH] = vector;
endtask

task insert_over_range;
  input  data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_OVER_RANGE +: RR_SPD_OVER_RANGE_1_WIDTH] = data;
endtask

task insert_record_bits;
  input [RR_SPD_NUM_RECORDBITS-1:0] data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + ch*RR_CH_WIDTH + RR_CH_POS_RC_BITS +: RR_SPD_NUM_RECORDBITS] = data;
endtask

task insert_record_cnt;
  input [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + ch*RR_CH_WIDTH + RR_CH_POS_RC_CNT +: RR_SPD_NUM_RECORD_COUNTER_BITS] = data;
endtask

task insert_user_id;
  input [RR_SPD_USER_ID_WIDTH_BITS-1:0] data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_USER_ID +: RR_SPD_USER_ID_WIDTH_BITS] = data;
endtask

task insert_general_purpose_vector;
  input [RR_SPD_GENERAL_PURPOSE_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_FULL)
    user_bus_o[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_GP_BITS +: RR_SPD_GENERAL_PURPOSE_WIDTH] = data;
endtask

// Gate count
task insert_timestamp_sync_cnt;
   input [RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] vector;
   input integer ch;
   user_bus_o[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_TIMESTAMP_SYNC_CNT +: RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH] = vector;
endtask

// Virtual channels
task insert_lowrate_ch_all;
   input [SPD_PARALLEL_SAMPLES_LOWRATE*SPD_DATAWIDTH_BITS-1:0] data;
   input integer ch;
   begin
      user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +:
                 SPD_PARALLEL_SAMPLES_LOWRATE*SPD_DATAWIDTH_BITS] = data;
   end
endtask

task insert_lowrate_ch_timestamp;
   input [RR_SPD_TIMESTAMP_WIDTH_BITS-1:0] timestamp;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch*RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_TS +: RR_SPD_TIMESTAMP_WIDTH_BITS] = timestamp;
endtask

task insert_lowrate_ch_trig_inhibit;
   input [RR_SPD_TRIGGER_INHIBIT_BITS-1:0] trig_inhibit;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch*RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_TRIGINH +: RR_SPD_TRIGGER_INHIBIT_BITS] = trig_inhibit;
endtask

task insert_lowrate_ch_timestampsync;
   input [RR_SPD_TSSYNC_BITS-1:0] tssync;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch*RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_TSSYNC +: RR_SPD_TSSYNC_BITS] = tssync;
endtask

task insert_lowrate_ch_trig;
   input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
   input integer ch;
   begin
      user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                 SPD_PARALLEL_SAMPLES_LOWRATE*SPD_DATAWIDTH_BITS +:
                 RR_CH_TRIG_VECTOR_1_WIDTH] = data;
   end
endtask


task insert_lowrate_ch_trig_rnum;
  input [RR_CH_TRIG_NUM_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_RNUM_POS +: RR_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_rrising;
  input [RR_CH_TRIG_RISING_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_RRISING_POS  +: RR_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_revent;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_REVENT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_tnum;
  input [RR_CH_TRIG_NUM_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_TNUM_POS +: RR_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_trising;
  input [RR_CH_TRIG_RISING_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_TRISING_POS  +: RR_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_tevent;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_TEVENT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_revent_pt;
  input [RR_CH_TRIG_EVENT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_REVENT_PT_POS  +: RR_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_extended_precision;
  input [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_EXTPREC_POS  +: RR_CH_TRIG_EXT_PREC_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_tevent_bitvect;
  input [RR_CH_TRIG_BITVECT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_TEVENT_BITVECT_POS  +: RR_CH_TRIG_BITVECT_WIDTH] = vector;
endtask

task insert_lowrate_ch_trig_revent_bitvect;
  input [RR_CH_TRIG_BITVECT_WIDTH-1:0] vector;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + RR_CH_LOWRATE_WIDTH*ch + RR_CH_LOWRATE_POS_CHTRIG + RR_CH_TRIG_REVENT_BITVECT_POS  +: RR_CH_TRIG_BITVECT_WIDTH] = vector;
endtask

task insert_lowrate_ch_record_bits;
   input [RR_SPD_NUM_RECORDBITS-1:0] data;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
              RR_CH_LOWRATE_POS_RC_BITS +: RR_SPD_NUM_RECORDBITS] = data;
endtask

task insert_lowrate_ch_record_cnt;
   input [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] data;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
              RR_CH_LOWRATE_POS_RC_CNT +: RR_SPD_NUM_RECORD_COUNTER_BITS] = data;

endtask

task insert_lowrate_ch_user_id;
  input [RR_SPD_USER_ID_WIDTH_BITS-1:0] data;
  input integer ch;
  if(ch < SPD_PROCESSING_CHANNELS_LOWRATE)
    user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
               RR_CH_LOWRATE_POS_USER_ID +: RR_SPD_USER_ID_WIDTH_BITS] = data;
endtask

task insert_lowrate_ch_over_range;
   input  [RR_SPD_OVER_RANGE_1_WIDTH-1:0] data;
   input integer ch;
   begin
      user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                 RR_CH_LOWRATE_POS_OVER_RANGE +: RR_SPD_OVER_RANGE_1_WIDTH] = data;
   end
endtask

// Gate count
task insert_lowrate_ch_timestamp_sync_cnt;
   input [RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] vector;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
              RR_CH_LOWRATE_POS_TIMESTAMP_SYNC_CNT +: RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH] = vector;
endtask

task insert_lowrate_ch_general_purpose_vector;
   input [RR_SPD_GENERAL_PURPOSE_WIDTH-1:0] vector;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
              RR_CH_LOWRATE_POS_GP_BITS +: RR_SPD_GENERAL_PURPOSE_WIDTH] = vector;
endtask

task insert_lowrate_ch_data_valid;
   input valid;
   input integer ch;
   user_bus_o[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
              RR_CH_LOWRATE_WIDTH -1 +: 1] = valid;
endtask


`endif // DISABLE_OUTPUT_RR

//Input
`ifndef  DISABLE_INPUT_RR

wire [RR_DATA_BUS_WIDTH:0] user_bus_i = {s_axis_tvalid, s_axis_tdata};

function [RR_AUX_TRIG_VECTOR_WIDTH-1:0] extract_aux_trig;
   input integer        dummy;
   extract_aux_trig =   user_bus_i[RR_POS_AUX_TRIG +: RR_AUX_TRIG_VECTOR_WIDTH];
endfunction

// Aux trig fields
// Reset event
function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_aux_trig_rnum;
   input integer        dummy;
   extract_aux_trig_rnum = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_RNUM_POS +: RR_CH_TRIG_NUM_WIDTH];
endfunction
function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_aux_trig_rrising;
   input integer        dummy;
   extract_aux_trig_rrising = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_RRISING_POS +: RR_CH_TRIG_RISING_WIDTH];
endfunction
function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_aux_trig_revent;
   input integer        dummy;
   extract_aux_trig_revent = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_REVENT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction
// Trigger event
function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_aux_trig_tnum;
   input integer        dummy;
   extract_aux_trig_tnum = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_TNUM_POS +: RR_CH_TRIG_NUM_WIDTH];
endfunction
function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_aux_trig_trising;
   input integer        dummy;
   extract_aux_trig_trising = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_TRISING_POS +: RR_CH_TRIG_RISING_WIDTH];
endfunction
function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_aux_trig_tevent;
   input integer        dummy;
   extract_aux_trig_tevent = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_TEVENT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction
// Reset event (pre-trigger)
function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_aux_trig_revent_pt;
   input integer        dummy;
   extract_aux_trig_revent_pt = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_REVENT_PT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction
// Sample skip
function [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] extract_aux_trig_extended_precision;
   input integer        dummy;
   extract_aux_trig_extended_precision = user_bus_i[RR_POS_AUX_TRIG + RR_CH_TRIG_EXTPREC_POS +: RR_CH_TRIG_EXT_PREC_WIDTH];
endfunction

function [RR_CH_WIDTH-1:0] extract_ch;
   input integer ch;
   case (ch)
     0:                             extract_ch = user_bus_i[RR_CH_FIRST_BIT + CH_A * RR_CH_WIDTH +: RR_CH_WIDTH];
     1:if (SPD_PROCESSING_CHANNELS_FULL > 1) extract_ch = user_bus_i[RR_CH_FIRST_BIT + CH_B * RR_CH_WIDTH +: RR_CH_WIDTH];
     2:if (SPD_PROCESSING_CHANNELS_FULL > 2) extract_ch = user_bus_i[RR_CH_FIRST_BIT + CH_C * RR_CH_WIDTH +: RR_CH_WIDTH];
     3:if (SPD_PROCESSING_CHANNELS_FULL > 3) extract_ch = user_bus_i[RR_CH_FIRST_BIT + CH_D * RR_CH_WIDTH +: RR_CH_WIDTH];

   endcase
endfunction

// Samples
function [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] extract_ch_all_samples_from_ch_data;
   input [RR_CH_WIDTH-1:0] data; // returned by "extract_ch(ch)"
   extract_ch_all_samples_from_ch_data = data[0 +: SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS];
endfunction

function [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] extract_ch_all;
   input integer       ch;
   extract_ch_all = extract_ch_all_samples_from_ch_data(extract_ch(ch));
endfunction

// Data valid
function extract_data_valid_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_data_valid_from_ch_data = data[RR_CH_WIDTH-1];
endfunction

function extract_data_valid;
   input integer        ch;
   extract_data_valid = extract_data_valid_from_ch_data(extract_ch(ch));
endfunction

// Timestamp

function [RR_SPD_TIMESTAMP_WIDTH_BITS-1:0] extract_timestamp;
   input integer ch;
   if(ch < SPD_PROCESSING_CHANNELS_FULL)
     extract_timestamp = user_bus_i[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_TS +: RR_SPD_TIMESTAMP_WIDTH_BITS];
endfunction

function [RR_SPD_TRIGGER_INHIBIT_BITS-1:0] extract_trig_inhibit;
   input integer ch;
   if(ch < SPD_PROCESSING_CHANNELS_FULL)
     extract_trig_inhibit = user_bus_i[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_TRIGINH +: RR_SPD_TRIGGER_INHIBIT_BITS];
endfunction

function [RR_SPD_TSSYNC_BITS-1:0] extract_timestampsync;
   input integer ch;
   if(ch < SPD_PROCESSING_CHANNELS_FULL)
     extract_timestampsync = user_bus_i[RR_CH_FIRST_BIT + ch * RR_CH_WIDTH + RR_CH_POS_TSSYNC +: RR_SPD_TSSYNC_BITS];
endfunction

//CH trig
function [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] extract_ch_trig_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_trig_from_ch_data = data[SPD_PARALLEL_SAMPLES * SPD_DATAWIDTH_BITS +: RR_CH_TRIG_VECTOR_1_WIDTH];
endfunction

function [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] extract_ch_trig;
   input integer        ch;
   extract_ch_trig = extract_ch_trig_from_ch_data(extract_ch(ch));
endfunction

// CH trig fields
// rnum
function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_ch_trig_rnum_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_rnum_from_trig_data = data[RR_CH_TRIG_RNUM_POS +: RR_CH_TRIG_NUM_WIDTH];
endfunction

function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_ch_trig_rnum;
  input integer ch;
  extract_ch_trig_rnum = extract_ch_trig_rnum_from_trig_data(extract_ch_trig(ch));
endfunction

//rrising
function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_ch_trig_rrising_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_rrising_from_trig_data = data[RR_CH_TRIG_RRISING_POS +: RR_CH_TRIG_RISING_WIDTH];
endfunction

function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_ch_trig_rrising;
  input integer ch;
  extract_ch_trig_rrising = extract_ch_trig_rrising_from_trig_data(extract_ch_trig(ch));
endfunction

// revent
function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_revent_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_revent_from_trig_data = data[RR_CH_TRIG_REVENT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction

function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_revent;
  input integer ch;
  extract_ch_trig_revent = extract_ch_trig_revent_from_trig_data(extract_ch_trig(ch));
endfunction

// tnum
function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_ch_trig_tnum_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_tnum_from_trig_data = data[RR_CH_TRIG_TNUM_POS +: RR_CH_TRIG_NUM_WIDTH];
endfunction

function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_ch_trig_tnum;
  input integer ch;
  extract_ch_trig_tnum = extract_ch_trig_tnum_from_trig_data(extract_ch_trig(ch));
endfunction

// trising
function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_ch_trig_trising_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_trising_from_trig_data = data[RR_CH_TRIG_TRISING_POS +: RR_CH_TRIG_RISING_WIDTH];
endfunction

function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_ch_trig_trising;
  input integer ch;
  extract_ch_trig_trising = extract_ch_trig_trising_from_trig_data(extract_ch_trig(ch));
endfunction

// tevent
function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_tevent_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_tevent_from_trig_data = data[RR_CH_TRIG_TEVENT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction

function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_tevent;
  input integer ch;
  extract_ch_trig_tevent = extract_ch_trig_tevent_from_trig_data(extract_ch_trig(ch));
endfunction

// revent (pretrigg)
function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_revent_pt_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_revent_pt_from_trig_data = data[RR_CH_TRIG_REVENT_PT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction

function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_revent_pt;
  input integer ch;
  extract_ch_trig_revent_pt = extract_ch_trig_revent_pt_from_trig_data(extract_ch_trig(ch));
endfunction

// sample skip factor
function [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] extract_ch_trig_extended_precision_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_extended_precision_from_trig_data = data[RR_CH_TRIG_EXTPREC_POS +: RR_CH_TRIG_EXT_PREC_WIDTH];
endfunction

function [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] extract_ch_trig_extended_precision;
  input integer ch;
  extract_ch_trig_extended_precision = extract_ch_trig_extended_precision_from_trig_data(extract_ch_trig(ch));
endfunction

// Channel trig bit vector
function [RR_CH_TRIG_BITVECT_WIDTH-1:0] extract_ch_trig_tevent_bitvect_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_tevent_bitvect_from_trig_data = data[RR_CH_TRIG_TEVENT_BITVECT_POS +: RR_CH_TRIG_BITVECT_WIDTH];
endfunction

function [RR_CH_TRIG_BITVECT_WIDTH-1:0] extract_ch_trig_tevent_bitvect;
  input integer ch;
  extract_ch_trig_tevent_bitvect = extract_ch_trig_tevent_bitvect_from_trig_data(extract_ch_trig(ch));
endfunction

function [RR_CH_TRIG_BITVECT_WIDTH-1:0] extract_ch_trig_revent_bitvect_from_trig_data;
  input [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  extract_ch_trig_revent_bitvect_from_trig_data = data[RR_CH_TRIG_REVENT_BITVECT_POS +: RR_CH_TRIG_BITVECT_WIDTH];
endfunction

function [RR_CH_TRIG_BITVECT_WIDTH-1:0] extract_ch_trig_revent_bitvect;
  input integer ch;
  extract_ch_trig_revent_bitvect = extract_ch_trig_revent_bitvect_from_trig_data(extract_ch_trig(ch));
endfunction

// overrange
function  extract_ch_over_range_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_over_range_from_ch_data = data[RR_CH_POS_OVER_RANGE +: 1];
endfunction

function extract_over_range;
   input integer        ch;
   extract_over_range = extract_ch_over_range_from_ch_data(extract_ch(ch));
endfunction

// record bits
function [RR_SPD_NUM_RECORDBITS-1:0] extract_ch_record_bits_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_record_bits_from_ch_data = data[RR_CH_POS_RC_BITS +: RR_SPD_NUM_RECORDBITS];
endfunction

function [RR_SPD_NUM_RECORDBITS-1:0] extract_record_bits;
   input integer        ch;
   extract_record_bits = extract_ch_record_bits_from_ch_data(extract_ch(ch));
endfunction

// record counter
function [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] extract_ch_record_cnt_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_record_cnt_from_ch_data = data[RR_CH_POS_RC_CNT +: RR_SPD_NUM_RECORD_COUNTER_BITS];
endfunction

function [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] extract_record_cnt;
   input integer        ch;
   extract_record_cnt = extract_ch_record_cnt_from_ch_data(extract_ch(ch));
endfunction

// user ID
function  [RR_SPD_USER_ID_WIDTH_BITS-1:0] extract_ch_user_id_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_user_id_from_ch_data = data[RR_CH_POS_USER_ID +: RR_SPD_USER_ID_WIDTH_BITS];
endfunction

function [RR_SPD_USER_ID_WIDTH_BITS-1:0] extract_user_id;
   input integer        ch;
   extract_user_id = extract_ch_user_id_from_ch_data(extract_ch(ch));
endfunction

// general purpose
function  [RR_SPD_GENERAL_PURPOSE_WIDTH-1:0] extract_ch_general_purpose_vector_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_general_purpose_vector_from_ch_data = data[RR_CH_POS_GP_BITS +: RR_SPD_GENERAL_PURPOSE_WIDTH];
endfunction

function [RR_SPD_GENERAL_PURPOSE_WIDTH-1:0] extract_ch_general_purpose_vector;
   input integer        ch;
   extract_ch_general_purpose_vector = extract_ch_general_purpose_vector_from_ch_data(extract_ch(ch));
endfunction

function [RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] extract_ch_timestamp_sync_cnt_from_ch_data;
   input [RR_CH_WIDTH-1:0] data;
   extract_ch_timestamp_sync_cnt_from_ch_data =  data[RR_CH_POS_TIMESTAMP_SYNC_CNT +: RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH];
endfunction

function [RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] extract_timestamp_sync_cnt;
   input integer ch;
   extract_timestamp_sync_cnt =  extract_ch_timestamp_sync_cnt_from_ch_data(extract_ch(ch));
endfunction

// Virtual channels
function [SPD_PARALLEL_SAMPLES_LOWRATE*SPD_DATAWIDTH_BITS-1:0] extract_lowrate_ch_all;
   input integer ch;
   extract_lowrate_ch_all = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +:
                                    SPD_PARALLEL_SAMPLES_LOWRATE*SPD_DATAWIDTH_BITS];
endfunction

function [RR_SPD_TIMESTAMP_WIDTH_BITS-1:0] extract_lowrate_ch_timestamp;
   input integer ch;
   if(ch < SPD_PROCESSING_CHANNELS_FULL)
     extract_lowrate_ch_timestamp = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_TS +: RR_SPD_TIMESTAMP_WIDTH_BITS];
endfunction

function [RR_SPD_TRIGGER_INHIBIT_BITS-1:0] extract_lowrate_ch_trig_inhibit;
   input integer ch;
   if(ch < SPD_PROCESSING_CHANNELS_FULL)
     extract_lowrate_ch_trig_inhibit = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_TRIGINH +: RR_SPD_TRIGGER_INHIBIT_BITS];
endfunction

function [RR_SPD_TSSYNC_BITS-1:0] extract_lowrate_ch_timestampsync;
   input integer ch;
   if(ch < SPD_PROCESSING_CHANNELS_FULL)
     extract_lowrate_ch_timestampsync = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_TSSYNC +: RR_SPD_TSSYNC_BITS];
endfunction

function [RR_CH_TRIG_VECTOR_1_WIDTH-1:0] extract_lowrate_ch_trig;
   input integer        ch;
   extract_lowrate_ch_trig = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                                        RR_CH_LOWRATE_POS_CHTRIG +: RR_CH_TRIG_VECTOR_1_WIDTH];
endfunction

function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_lowrate_ch_trig_rnum;
   input integer ch;
   extract_lowrate_ch_trig_rnum = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_RNUM_POS +: RR_CH_TRIG_NUM_WIDTH];
endfunction

function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_lowrate_ch_trig_rrising;
   input integer ch;
   extract_lowrate_ch_trig_rrising = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_RRISING_POS +: RR_CH_TRIG_RISING_WIDTH];
endfunction

function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_lowrate_ch_trig_revent;
   input integer ch;
   extract_lowrate_ch_trig_revent = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_REVENT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction

function [RR_CH_TRIG_NUM_WIDTH-1:0] extract_lowrate_ch_trig_tnum;
   input integer ch;
   extract_lowrate_ch_trig_tnum = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_TNUM_POS +: RR_CH_TRIG_NUM_WIDTH];
endfunction

function [RR_CH_TRIG_RISING_WIDTH-1:0] extract_lowrate_ch_trig_trising;
   input integer ch;
   extract_lowrate_ch_trig_trising = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_TRISING_POS +: RR_CH_TRIG_RISING_WIDTH];
endfunction

function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_lowrate_ch_trig_tevent;
   input integer ch;
   extract_lowrate_ch_trig_tevent = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_TEVENT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction

function [RR_CH_TRIG_EVENT_WIDTH-1:0] extract_lowrate_ch_trig_revent_pt;
   input integer ch;
   extract_lowrate_ch_trig_revent_pt = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_REVENT_PT_POS +: RR_CH_TRIG_EVENT_WIDTH];
endfunction

function [RR_CH_TRIG_EXT_PREC_WIDTH-1:0] extract_lowrate_ch_trig_extended_precision;
   input integer ch;
   extract_lowrate_ch_trig_extended_precision = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_EXTPREC_POS +: RR_CH_TRIG_EXT_PREC_WIDTH];
endfunction

// Channel trig bit vector
function [RR_CH_TRIG_BITVECT_WIDTH-1:0] extract_lowrate_ch_trig_tevent_bitvect;
  input integer ch;
  extract_lowrate_ch_trig_tevent_bitvect = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_TEVENT_BITVECT_POS +: RR_CH_TRIG_BITVECT_WIDTH];
endfunction

function [RR_CH_TRIG_BITVECT_WIDTH-1:0] extract_lowrate_ch_trig_revent_bitvect;
  input integer ch;
  extract_lowrate_ch_trig_revent_bitvect = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH + RR_CH_LOWRATE_POS_CHTRIG +
                                            RR_CH_TRIG_REVENT_BITVECT_POS +: RR_CH_TRIG_BITVECT_WIDTH];
endfunction



function [RR_SPD_NUM_RECORDBITS-1:0] extract_lowrate_ch_record_bits;
   input integer ch;
   extract_lowrate_ch_record_bits = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                                            RR_CH_LOWRATE_POS_RC_BITS +: RR_SPD_NUM_RECORDBITS];
endfunction

function [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] extract_lowrate_ch_record_cnt;
   input integer ch;
   extract_lowrate_ch_record_cnt = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                                           RR_CH_LOWRATE_POS_RC_CNT +: RR_SPD_NUM_RECORD_COUNTER_BITS];
endfunction

function [RR_SPD_USER_ID_WIDTH_BITS-1:0] extract_lowrate_ch_user_id;
   input integer        ch;
   extract_lowrate_ch_user_id = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                                           RR_CH_LOWRATE_POS_USER_ID +: RR_SPD_USER_ID_WIDTH_BITS];
endfunction

function [RR_SPD_OVER_RANGE_1_WIDTH-1:0] extract_lowrate_ch_over_range;
   input integer ch;
   extract_lowrate_ch_over_range = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                                           RR_CH_LOWRATE_POS_OVER_RANGE +: RR_SPD_OVER_RANGE_1_WIDTH];
endfunction

function [RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] extract_lowrate_ch_timestamp_sync_cnt;
   input integer ch;
   extract_lowrate_ch_timestamp_sync_cnt = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch *RR_CH_LOWRATE_WIDTH +
                                            RR_CH_LOWRATE_POS_TIMESTAMP_SYNC_CNT +: RR_SPD_TIMESTAMP_SYNC_CNT_WIDTH];
endfunction

function [RR_SPD_GENERAL_PURPOSE_WIDTH-1:0] extract_lowrate_ch_general_purpose_vector;
   input integer ch;
   extract_lowrate_ch_general_purpose_vector = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch *RR_CH_LOWRATE_WIDTH +
                                            RR_CH_LOWRATE_POS_GP_BITS +: RR_SPD_GENERAL_PURPOSE_WIDTH];
endfunction

function extract_lowrate_ch_data_valid;
   input integer ch;
   extract_lowrate_ch_data_valid = user_bus_i[RR_CH_LOWRATE_FIRST_BIT + ch * RR_CH_LOWRATE_WIDTH +
                                              RR_CH_LOWRATE_WIDTH-1];
endfunction

`endif
