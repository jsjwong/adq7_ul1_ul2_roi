`timescale 1ns / 1ps
`default_nettype none
//////////////////////////////////////////////////////////////////////////////////
// Author: Justin S. J. Wong
// 
// Create Date: 04.09.2020
// Design Name: 
// Module Name: line_BRAM_sp
// Project Name: 
// Target Devices: 
// Tool Versions: 2017.1
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module line_bram_sp  #(
	parameter DataWidth = 32*16,
	parameter MemAddrWidth = 4,      
	parameter MemAddrNum = 2**MemAddrWidth,
	parameter UseOutRegA = 0
)(
	input  wire iClk_A, iEN_A, iWE_A,
	input  wire [MemAddrWidth-1:0] iAddrA,
	input  wire [DataWidth-1:0] iDatA,
	output wire [DataWidth-1:0] oDatA
);

(* RAM_STYLE= "BLOCK" *) reg [DataWidth-1:0] ram [0:MemAddrNum-1];
reg [DataWidth-1:0] rDatA [1:0];

//******************************************************************
always @ (posedge iClk_A)
begin

	if (iEN_A) begin
		//****** Mode: READ_FIRST (Read-before-write Mode) Slower timing ***********
		if (iWE_A) ram[iAddrA] <= iDatA;
		rDatA[0] <= ram[iAddrA];

		if(UseOutRegA) begin: OUT_REG_A
			rDatA[1] <= rDatA[0];
		end
	end
	
end
assign oDatA = rDatA[UseOutRegA];

endmodule
`default_nettype wire

