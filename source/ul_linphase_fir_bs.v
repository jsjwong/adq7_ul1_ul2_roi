/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: Linear phase FIR filter barrel shifter to compensate for
 *              the group delay. Barrel shifter propagation delay is
 *              1 + ORDER / (2 * PARALLEL_SAMPLES) data valid cycles + one
 *              clock cycle, i.e. setup of one cycle and propagation delay
 *              of one cycle. An even order filter is required to align the
 *              group delay with the sample grid.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_linphase_fir_bs #(
   DATAWIDTH_BITS   = 0,
   PARALLEL_SAMPLES = 0,
   ORDER            = 0,
   SETUP_CHECKER    = 0
)(
   input wire clk_i,
   input wire rst_i,

   /* Input data */
   input wire [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_i,
   input wire data_valid_i,

   /* Output data */
   output wire [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_o,
   output wire data_valid_o
);
   `include "ul_clog2.vh"

   /* Local parameters, wires & registers */
   localparam SETUP = 2 + ORDER / (2 * PARALLEL_SAMPLES);
   localparam TOTAL_DELAY = SETUP;
   localparam TOTAL_DELAY_CLOG2 = clog2(TOTAL_DELAY);
   localparam BS_IDX = (ORDER / 2) % PARALLEL_SAMPLES;

   /* Parameter validation */
   if (SETUP_CHECKER != SETUP)
      always LINPHASE_FIR_BS_SETUP_CHECKER_WRONG_VALUE();
   if ((ORDER % 2) != 0)
      always ERROR_LINPHASE_FIR_BS_NOT_EVEN_ORDER();

   reg [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_d;
   wire [2*DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] barrel;
   reg [DATAWIDTH_BITS*PARALLEL_SAMPLES-1:0] data_out;

   reg [TOTAL_DELAY_CLOG2-1:0] data_valid_ctr = {(TOTAL_DELAY_CLOG2){1'b0}};
   wire data_valid_ctr_max_reached;

   /* Signal replication of the driving registers to avoid high fanout. */
   (* max_fanout = 256 *) wire data_valid_in;
   assign data_valid_in = data_valid_i;

   /* One-cycle memory */
   always @(posedge clk_i) begin
      if (data_valid_in)
         data_d <= data_i;
   end

   /* Barrel */
   assign barrel = {data_i, data_d};

   /* Slice the appropriate range from the barrel. */
   always @(posedge clk_i) begin
      if (data_valid_in)
         data_out <=
            barrel[BS_IDX*DATAWIDTH_BITS +: DATAWIDTH_BITS*PARALLEL_SAMPLES];
   end

   /* Data valid generation */
   always @(posedge clk_i) begin
      if (rst_i) begin
         data_valid_ctr <= {(TOTAL_DELAY_CLOG2){1'b0}};
      end else begin
         if (data_valid_ctr_max_reached & ~data_valid_in)
            data_valid_ctr <= data_valid_ctr
                              - {{(TOTAL_DELAY_CLOG2-1){1'b0}}, 1'b1};
         else if (~data_valid_ctr_max_reached & data_valid_in)
            data_valid_ctr <= data_valid_ctr
                              + {{(TOTAL_DELAY_CLOG2-1){1'b0}}, 1'b1};
      end
   end
   assign data_valid_ctr_max_reached = (data_valid_ctr == TOTAL_DELAY);

   /* Assign outputs */
   assign data_o = data_out;
   assign data_valid_o = data_valid_ctr_max_reached;

`ifdef XILINX_SIMULATOR
   wire [DATAWIDTH_BITS-1:0] data_i_s [PARALLEL_SAMPLES-1:0];
   wire [DATAWIDTH_BITS-1:0] data_o_s [PARALLEL_SAMPLES-1:0];
   wire [DATAWIDTH_BITS-1:0] barrel_s [2*PARALLEL_SAMPLES-1:0];
   genvar k;
   generate
      for (k = 0; k < PARALLEL_SAMPLES; k = k + 1) begin
         assign data_i_s[k] = data_i[k*DATAWIDTH_BITS +: DATAWIDTH_BITS];
         assign data_o_s[k] = data_o[k*DATAWIDTH_BITS +: DATAWIDTH_BITS];
      end
      for (k = 0; k < 2*PARALLEL_SAMPLES; k = k + 1) begin
         assign barrel_s[k] = barrel[k*DATAWIDTH_BITS +: DATAWIDTH_BITS];
      end
   endgenerate
`endif

endmodule

`default_nettype wire
