/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description   : User logic 2 module
 * Documentation :
 *
 */

`timescale 1 ns / 1 ps

`default_nettype none
`include "user_logic2_defines.vh"

module user_logic2 # (
   // Do not modify the parameters beyond this line
   parameter integer CH_TRIG_DATA_WIDTH =
      `UL2_SPD_ANALOG_CHANNELS * `UL2_SPD_DATAWIDTH_BITS
      * `UL2_SPD_PARALLEL_SAMPLES,
   parameter integer CH_TRIG_VECTOR_WIDTH =
      `UL2_SPD_ANALOG_CHANNELS
      * (`UL2_SPD_NUM_CH_TRIG_BITS + `UL2_SPD_NUM_TRIG_ADDBITS
         +`UL2_SPD_NUM_TRIG_DATARD_ADDFRACBITS),
   parameter integer ADDR_WIDTH = 14
) (
   input wire                             clk_reg_i,
   input wire                             rst_i,
   input wire                             wr_i,
   output wire                            wr_ack_o,
   input wire [ADDR_WIDTH-1:0]            addr_i,
   input wire [31:0]                      wr_data_i,
   input wire                             rd_i,
   output wire                            rd_ack_o,
   output wire [31:0]                     rd_data_o,

   // Ports of AXI-S Slave Bus Interface s_axis
   input wire                             s_axis_aclk,
   input wire                             s_axis_aresetn,
   input wire [`UL2_DATA_BUS_WIDTH-1 : 0] s_axis_tdata,
   input wire                             s_axis_tvalid,

   // Ports of AXI-S Master Bus Interface m_axis
   input wire                             m_axis_aclk,
   input wire                             m_axis_aresetn,
   output reg                             m_axis_tvalid,
   output reg [`UL2_DATA_BUS_WIDTH-1 : 0] m_axis_tdata,
   input wire                             m_axis_tready,

   // GPIO
   // To board
   input wire [11:0]                      gpio_in_i,
   output wire [11:0]                     gpio_out_o,
   output wire [5:0]                      gpio_dir_o,

   input wire [3:0]                       gpdi_in_i,
   output wire [2:0]                      gpdo_out_o,

   input wire                             gpio_trig_in_i,
   output wire                            gpio_trig_out_o,
   output wire                            gpio_trig_dir_o,

   input wire                             gpio_sync_in_i,
   output wire                            gpio_sync_out_o,
   output wire                            gpio_sync_dir_o,


   // From CPU
   output wire [11:0]                     gpio_in_o,
   input wire [11:0]                      gpio_out_i,
   input wire [5:0]                       gpio_dir_i,

   output wire [3:0]                      gpdi_in_o,
   input wire [2:0]                       gpdo_out_i,

   output wire                            gpio_trig_in_o,
   input wire                             gpio_trig_out_i,
   input wire                             gpio_trig_dir_i,

   output wire                            gpio_sync_in_o,
   input wire                             gpio_sync_out_i,
   input wire                             gpio_sync_dir_i,

   // DRAM ports
   input wire                             clk_mem_i,

   output wire [511:0]                    write1_data_o,
   input wire                             write1_done_i,
   output wire                            write1_empty_o,
   output wire [31:0]                     write1_first_addr_o,
   output wire [31:0]                     write1_last_addr_o,
   output wire                            write1_last_o,
   input wire                             write1_read_i,
   output wire                            write1_reset_o,
   output wire                            write1_strobe_o,

   output wire [511:0]                    write2_data_o,
   input wire                             write2_done_i,
   output wire                            write2_empty_o,
   output wire [31:0]                     write2_first_addr_o,
   output wire [31:0]                     write2_last_addr_o,
   output wire                            write2_last_o,
   input wire                             write2_read_i,
   output wire                            write2_reset_o,
   output wire                            write2_strobe_o,

   output wire                            read1_abort_o,
   output wire                            read1_afull_o,
   input wire [511:0]                     read1_data_i,
   input wire                             read1_done_i,
   output wire [31:0]                     read1_first_addr_o,
   input wire                             read1_firstdata_i,
   output wire [31:0]                     read1_high_addr_o,
   output wire [31:0]                     read1_last_addr_o,
   input wire                             read1_lastdata_i,
   output wire [31:0]                     read1_low_addr_o,
   output wire                            read1_reset_o,
   input wire                             read1_sent_i,
   output wire                            read1_strobe_o,
   input wire                             read1_wr_i,

   output wire                            read2_abort_o,
   output wire                            read2_afull_o,
   input wire [511:0]                     read2_data_i,
   input wire                             read2_done_i,
   output wire [31:0]                     read2_first_addr_o,
   input wire                             read2_firstdata_i,
   output wire [31:0]                     read2_high_addr_o,
   output wire [31:0]                     read2_last_addr_o,
   input wire                             read2_lastdata_i,
   output wire [31:0]                     read2_low_addr_o,
   output wire                            read2_reset_o,
   input wire                             read2_sent_i,
   output wire                            read2_strobe_o,
   input wire                             read2_wr_i,
   input wire [127:0]                     license_bits_i,
   input wire                             license_valid_i
);
   // The BUS_PIPELINE value must always be set equal to the latency of your
   // data processing in this module in order to synchronize unused bus signals.
   localparam BUS_PIPELINE = 4;

   // These includes are need to extract data from the AXIS bus
`include "device_param.vh"
`include "bus_splitter_rr.vh"

//***** User includes *****************************************************************
`include "GetBitWidth.vh"

  // User application code
  wire [31:0] reg_0x10_in;
  wire [31:0] reg_0x11_in;
  wire [31:0] reg_0x12_in;
  wire [31:0] reg_0x13_in;

  wire [31:0] reg_0x10_out;
  wire [31:0] reg_0x11_out;
  wire [31:0] reg_0x12_out;
  wire [31:0] reg_0x13_out;



  // Note: This register file is an example. The source code is included so
  // that it can be modified by the user.
  ul2_regfile #(
    .ADDR_WIDTH(ADDR_WIDTH)
  ) regfile_inst (
    .clk(clk_reg_i),
    .rst_i(rst_i),
    .addr_i(addr_i),

    .wr_i(wr_i),
    .wr_ack_o(wr_ack_o),
    .wr_data_i(wr_data_i),

    .rd_i(rd_i),
    .rd_ack_o(rd_ack_o),
    .rd_data_o(rd_data_o),

    .reg_0x10_i(reg_0x10_in),
    .reg_0x11_i(reg_0x11_in),
    .reg_0x12_i(reg_0x12_in),
    .reg_0x13_i(reg_0x13_in),

    .reg_0x10_o(reg_0x10_out),
    .reg_0x11_o(reg_0x11_out),
    .reg_0x12_o(reg_0x12_out),
    .reg_0x13_o(reg_0x13_out)
  );


  //******** BRAM Line Buffer Parameters ***********************     
  localparam BlockNumMax = 1024/32; //Max number of Sample blocks per line (assume integer division round towards zero)
  localparam BlockCntWidth = GetBitWidth(BlockNumMax);
  localparam LineNumDef = 32; //Number of Lines

  localparam MemAddrNum = BlockNumMax * LineNumDef;
  localparam MemAddrWidth = GetBitWidth(MemAddrNum);

  localparam MarginLineNumWidth = 7; 
  localparam LineDelayCntWidth = 6;     
  localparam DetectThresholdWidth = 15;

  localparam RecordNumLimitWidth = 28; //0 = no limit
  localparam CellCountWidth = 24; //0 = no limit
  localparam CellCountTimerWidth = 22;


  //NOTE: Vivado requires two registers with ASYNC_REG="TRUE" constraint for clock domain crossing.
    
  (* ASYNC_REG="TRUE" *) reg [31:0]         CtrlReg16_in [1:0];
  (* ASYNC_REG="TRUE" *) reg [31:0]         CtrlReg17_in [1:0];
  (* ASYNC_REG="TRUE" *) reg [31:0]         CtrlReg18_in [1:0];
  (* ASYNC_REG="TRUE" *) reg [31:0]         CtrlReg19_in [1:0];

  reg [31:0]         CtrlReg16_out [1:0];
  reg [31:0]         CtrlReg17_out [1:0];
  reg [31:0]         CtrlReg18_out [1:0];
  reg [31:0]         CtrlReg19_out [1:0];

  //********* Control parameters assignment from control register buffers *********** 
  wire             clear_reg = CtrlReg16_in[1][0];
  wire  [3:0]      mode = CtrlReg16_in[1][4:1];
  wire             EnableSeg = CtrlReg16_in[1][5];
  wire             DebugDetect = CtrlReg16_in[1][6]; //Enable visual debugging of segmentation detection and counter signals

  wire       UseDelayedStart = CtrlReg16_in[1][7];    //Use delayed start
  wire       UseCellDetect = CtrlReg16_in[1][8];      //Use Cell detection
  wire       BackgroundSeg = CtrlReg16_in[1][9];      // Invert segmentation to select background instead of cells
  wire       IncludeLastCellSeg = CtrlReg16_in[1][10];//Extend RecordNumLimit slightly to include the last segment of cell 


  wire  [DetectThresholdWidth-1:0]   CellDetectThreshold; //Cell Detection Threshold
  wire  [LineDelayCntWidth-1:0]      LineDelaySel;        //(Must be 1 or higher) Select the number of lines to delay ouput
  wire  [MarginLineNumWidth-1:0]     MarginLineNum;       //(Must be 1 or higher) Select the number of lines to hold cell detection and margin after cells

  assign CellDetectThreshold = CtrlReg17_in[1][16+: DetectThresholdWidth];
  assign LineDelaySel =  CtrlReg17_in[1][8 +: LineDelayCntWidth];
  assign MarginLineNum = CtrlReg17_in[1][0 +: MarginLineNumWidth];

  wire  [RecordNumLimitWidth-1:0]    RecordNumLimit = CtrlReg18_in[1][RecordNumLimitWidth-1:0];
  wire                             ResetRecordCount = CtrlReg18_in[1][31];
  reg rNoRecordNumLimit = 1'b0;
  wire  [RecordNumLimitWidth-1:0]   RecordNumLimitDebug = CtrlReg19_in[1][RecordNumLimitWidth-1:0];
  wire                             UseRecordNumLimitDebug = CtrlReg19_in[1][31];

  reg   rUseDelayedStart = 1'b0; // Use delayed start
  reg   rUseCellDetect = 1'b0;   // Use Cell detection
  reg   rBackgroundSeg = 1'b0;   // Invert segmentation to select background instead of cells

  //********* User output registers **********************
  reg [RecordNumLimitWidth-1:0] record_cnt_total;
  reg [CellCountWidth-1:0] rCellCounter = 0;
  reg [CellCountWidth-1:0] rCellCountOut = 0;
  reg [CellCountWidth-1:0] rCellCounterContinuous = 0;
  reg [CellCountWidth-1:0] rCellCountContinuousOut = 0;
  reg [CellCountTimerWidth-1:0] rCellCountTimer = 0;

  assign reg_0x10_in = CtrlReg16_out[1];
  assign reg_0x11_in = CtrlReg17_out[1];
  assign reg_0x12_in = CtrlReg18_out[1];
  assign reg_0x13_in = 32'h12345678;
     
  always @ (posedge s_axis_aclk) begin //********* Control registers clock domain crossing *********** 

    rNoRecordNumLimit <= (RecordNumLimit==0);
    // Async Input control registers
    CtrlReg16_in[0] <= reg_0x10_out;
    CtrlReg16_in[1] <= CtrlReg16_in[0];
    
    CtrlReg17_in[0] <= reg_0x11_out;
    CtrlReg17_in[1] <= CtrlReg17_in[0];
    
    CtrlReg18_in[0] <= reg_0x12_out;
    CtrlReg18_in[1] <= CtrlReg18_in[0];
    
    CtrlReg19_in[0] <= reg_0x13_out;
    CtrlReg19_in[1] <= CtrlReg19_in[0];
    
    // Output control registers
    CtrlReg16_out[0] <= {{32-RecordNumLimitWidth{1'b0}},record_cnt_total};
    CtrlReg16_out[1] <= CtrlReg16_out[0];

    CtrlReg17_out[0] <= {{32-CellCountWidth{1'b0}},rCellCountOut};
    CtrlReg17_out[1] <= CtrlReg17_out[0];

    CtrlReg18_out[0] <= {{32-CellCountWidth{1'b0}},rCellCountContinuousOut};
    CtrlReg18_out[1] <= CtrlReg18_out[0];    
         
  end


  function [SPD_DATAWIDTH_BITS-1:0] ABS;  //Absolute value (remove sign from negative numbers)
    input signed [SPD_DATAWIDTH_BITS-1:0]  iA;
    ABS = ({SPD_DATAWIDTH_BITS{iA[SPD_DATAWIDTH_BITS-1]}} ^ iA) + iA[SPD_DATAWIDTH_BITS-1];//wSign_A ? (-iA) : iA; //
  endfunction

   //********* Custom logic registeres and wires *********** 
  reg [MemAddrWidth-1:0] WriteAddr = 0;
  reg [LineDelayCntWidth-1:0] LineWriteCount = 0;
  reg [LineDelayCntWidth-1:0] LineRecCount = 0;

  //*********************** Data sample signal, register, and buffer declarations *********************
  wire [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_comb = extract_ch_all(CH_A);
  reg  [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_in1;
  reg  [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_in2;   
  reg  [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_in3; 
  reg  [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_out;
  //reg [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] data_b_out;

  wire [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  wdata_a_buf;
  reg  [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  rdata_a_buf;

  //*********************** Control signal declarations *********************
  wire data_valid_a_comb = extract_data_valid(CH_A);
  reg [1:BUS_PIPELINE] data_valid_a_in = 0;
  reg                  data_valid_a_out = 1'b0;

  wire [RR_SPD_NUM_RECORDBITS-1:0] record_bits_a_comb = extract_record_bits(CH_A); //[0]: record start pulse, [1]: record end pulse
  reg [RR_SPD_NUM_RECORDBITS-1:0] record_bits_a_in [1:BUS_PIPELINE];
  reg [RR_SPD_NUM_RECORDBITS-1:0] record_bits_a_out = 2'b00; //[0]: record start pulse, [1]: record end pulse

  wire [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] record_cnt_a_comb = extract_record_cnt(CH_A);

  reg [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] record_cnt_a_in [1:BUS_PIPELINE];
  reg [RecordNumLimitWidth-1:0] record_cnt_a_custom;
  reg rRecord_cnt_over_limit = 1'b0;
  reg [RecordNumLimitWidth-1:0] record_cnt_debug;
  reg [RR_SPD_NUM_RECORD_COUNTER_BITS-1:0] record_cnt_a_out;


  // ********** Test pattern example code ************************************************
  reg  [15:0]   pattern_cnt; //To generate test pattern for debugging
  reg record_started = 1'b0;
  wire DataWriteEN = record_bits_a_comb[0] | record_started; 

  wire [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] data_a_x;
  reg [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_x2;
  reg [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0]  data_a_x3;


  wire [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] wdata_diff;
  wire [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] wdata_abs_diff;
  //wire [SPD_DATAWIDTH_BITS-1:0] wdata_line_mean;
  reg [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] rdata_diff;
  reg [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] rdata_abs_diff;
  wire [SPD_PARALLEL_SAMPLES-1:0] wCell_Detected;
  wire [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] rdata_detected;
  reg rCell_found = 1'b0;

  reg [MarginLineNumWidth-1:0] rContinuousDetectCntDn = 0;
  reg rContinuousDetectCnt_IsZero = 1'b0;
  reg rContinuousDetectCnt_IsOne = 1'b0;

  reg [MarginLineNumWidth-1:0] rCellDetectCntDn;
  reg rCellDetectCnt_IsZero = 1'b0;
  reg rCellDetectCnt_IsOne = 1'b0; 

  reg rCellExistWithinMargin = 1'b0;
  reg rCellDetectCntEN = 1'b0;

  reg rSuppressStartEdgeCell = 1'b0;
  reg rSuppressedStartRead = 1'b0;
  reg rSuppressedReadEN = 1'b0;

  reg HaltAcquisition = 1'b0;
  reg HaltAcquisitionDelayed = 1'b0;
  reg DebugHaltAcquisition = 1'b0;

  reg DelayedStartOfFrame_rs2 = 1'b0; //delayed by 3 cycles from initial Rec start (triggered by record_bits_a_in[2][0])
  reg StartRead_rs2 = 1'b0; //delayed by 3 cycles from initial Rec start (triggered by record_bits_a_in[2][0])
  reg StartedReading = 1'b0; //delayed by 3 cycles from initial Rec start (triggered by record_bits_a_in[2][0])
  reg StartReadDebug_rs2 = 1'b0;

  genvar gi;
  generate
      for (gi=0; gi<SPD_PARALLEL_SAMPLES; gi=gi+1) begin: gen_diff_subtractor
          assign wdata_diff[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS] =
            data_a_in2[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS] - wdata_a_buf[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS];
          assign wdata_abs_diff[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS] = ABS(wdata_diff[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS]);
                  
          assign wCell_Detected[gi] = (rdata_abs_diff[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS] >= CellDetectThreshold);
          assign rdata_detected[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS] = {8'd0,wCell_Detected[gi],7'd0};
      end
  endgenerate

  line_bram_sp #(
    .DataWidth(SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS),
    .MemAddrWidth(MemAddrWidth),      
    .MemAddrNum(MemAddrNum),
    .UseOutRegA(1)
  ) line_bram_inst (
    .iClk_A(s_axis_aclk), .iEN_A(1'b1), .iWE_A(DataWriteEN),
    .iDatA(data_a_comb),
    .iAddrA(WriteAddr),
    .oDatA(wdata_a_buf)
  );

  //*************************************************************
  always @ (posedge s_axis_aclk) begin //Generate control signals
    if(clear_reg) begin
      pattern_cnt <= 0;
      record_started <= 1'b0;
      LineWriteCount <= 0;
      LineRecCount <= 0;
      WriteAddr <= 0;
      rdata_diff <= 0;
      rdata_abs_diff <= 0;
      
      rCell_found <= 0;

      rContinuousDetectCntDn <= 0;
      rContinuousDetectCnt_IsZero <= 1'b0;
      rContinuousDetectCnt_IsOne <= 1'b0;
      
      rCellDetectCntDn <= 0;
      rCellDetectCnt_IsZero <= 1'b0;
      rCellDetectCnt_IsOne <= 1'b0; 
      rCellExistWithinMargin <= 1'b0;
      rSuppressStartEdgeCell <= 1'b0;
      rSuppressedStartRead <= 1'b0;
      rSuppressedReadEN <= 1'b0;
      
      begin: clear_pipeline
        integer t;
        for(t=1; t<=BUS_PIPELINE; t=t+1) begin
          record_bits_a_in[t] <= 0;
          record_cnt_a_in[t] <= 0;
        end
      end
      data_valid_a_out <=  1'b0;
      record_bits_a_out <= 2'b00;
      record_cnt_a_custom <= 0;
      record_cnt_total <= 0;
      rRecord_cnt_over_limit <= 1'b0;
      record_cnt_debug <= 0; //**debug
      record_cnt_a_out <= 0;
      rCellDetectCntEN <=  1'b0;
      
      DelayedStartOfFrame_rs2 <= 1'b0;
      StartRead_rs2 <= 1'b0;
      StartedReading <= 1'b0;
      StartReadDebug_rs2 <= 1'b0;//**debug
      HaltAcquisition <= 1'b0;
      HaltAcquisitionDelayed <= 1'b0;
      DebugHaltAcquisition <= 1'b0;//**debug
      
      rCellCounter <= 0;
      rCellCountOut <= 0;
      rCellCounterContinuous <= 0;
      rCellCountContinuousOut <= 0;
      rCellCountTimer <= 0;

    end else if(data_valid_a_comb) begin
            
      if (pattern_cnt<16'd9900) pattern_cnt <= pattern_cnt + 7'd100;
      else pattern_cnt <= 0;
      
      if (record_bits_a_comb[1]) begin
        record_started <= 1'b0;
      end else if (record_bits_a_comb[0]) begin
        record_started <= 1'b1;
      end

      //***** Generate BRAM write and read address based on LineWriteCount
      if (DataWriteEN) begin
        WriteAddr <= WriteAddr + 1'b1; //default
        if (record_bits_a_comb[1]) begin
          if (LineWriteCount < (LineDelaySel-1'b1)) begin
            LineWriteCount <= LineWriteCount + 1'b1;
          end else begin
            LineWriteCount <= 0;
            WriteAddr <= 0;
          end
        end
      end           

      //******** Continuous Cell detection and counting*****************              
      if (record_bits_a_in[3][1]) begin
        if ((wCell_Detected>0) || rCell_found) begin
          rContinuousDetectCntDn <= MarginLineNum;
        end else begin
          if (~rContinuousDetectCnt_IsZero) begin
            rContinuousDetectCntDn <= rContinuousDetectCntDn - 1'b1;
          end
        end
      end                    
      if (record_bits_a_in[2][0]) begin
        rCellCounterContinuous <= rCellCounterContinuous + rContinuousDetectCnt_IsOne; //default
        if (rCellCountTimer==0)begin
          rCellCounterContinuous <= 0;
          rCellCountContinuousOut <= rCellCounterContinuous + rContinuousDetectCnt_IsOne;
        end
        rCellCountTimer <= rCellCountTimer + 1'b1;
      end
      if (record_bits_a_in[3][0]) begin
        rContinuousDetectCnt_IsZero <= (rContinuousDetectCntDn==0); 
        rContinuousDetectCnt_IsOne <= (rContinuousDetectCntDn==1);              
      end                         
      //******** Generate Cell segmentation signal *****************
      if (~StartRead_rs2) begin
        rCellDetectCntDn <= 0;
        rCellDetectCnt_IsZero <= 1'b0;
        rCellDetectCnt_IsOne <= 1'b0; 
        rCellExistWithinMargin <= 1'b0;
        rSuppressStartEdgeCell <= 1'b0;
        rSuppressedStartRead <= 1'b0;
        rCellCounter <= 0;       
      end else if (record_bits_a_in[3][1]) begin
        if ((wCell_Detected>0) || rCell_found) begin
          rCellExistWithinMargin <= 1'b1;
          rCellDetectCntDn <= MarginLineNum;
          if (DelayedStartOfFrame_rs2) rSuppressStartEdgeCell <= 1'b1; //used to reject partially cropped cells at the starting edge
        end else begin
          if (DelayedStartOfFrame_rs2 || (rSuppressStartEdgeCell && rCellDetectCnt_IsOne))rSuppressedStartRead <= 1'b1;
          if (rCellDetectCnt_IsOne) rSuppressStartEdgeCell <= 1'b0; //dessert after first edge cell
          if (rCellDetectCnt_IsOne || rCellDetectCnt_IsZero) rCellExistWithinMargin <= 1'b0;
          
          if (~rCellDetectCnt_IsZero) begin
              rCellDetectCntDn <= rCellDetectCntDn - 1'b1;
          end

        end
      end

      if (record_bits_a_in[3][0]) begin
        rCellDetectCntEN <= rCellExistWithinMargin;//(rCellDetectCntDn>0);
        rSuppressedReadEN <= rSuppressedStartRead;
        rCellDetectCnt_IsZero <= (rCellDetectCntDn==0);
        rCellDetectCnt_IsOne <= (rCellDetectCntDn==1); //Indicate last line of cell segment            
      end           

      if (record_bits_a_in[3][0]) begin
        rCell_found <= (wCell_Detected>0);
      end else if (wCell_Detected>0) begin //keep track of any detected cell
        rCell_found <= 1'b1;
      end   
                    
      //*********Generate initial delayed StartRead signal and limit total record count************************************************************
      if (record_bits_a_in[2][0]) begin
        rUseDelayedStart <= UseDelayedStart & EnableSeg;//Use delayed start
        rUseCellDetect <= UseCellDetect & EnableSeg;//Use Cell detection
        rBackgroundSeg <= BackgroundSeg;// Invert segmentation to select background instead of cells
        if (ResetRecordCount) begin
          HaltAcquisition <= 1'b0;
          HaltAcquisitionDelayed <= 1'b0;
          DebugHaltAcquisition <= 1'b0;//**debug
          LineRecCount <= 0;
          
          rCellDetectCntEN <= 1'b0;
          rSuppressedReadEN <= 1'b0;
          
          DelayedStartOfFrame_rs2 <= 1'b0;
          StartRead_rs2 <= 1'b0;
          StartedReading <= 1'b0;
          StartReadDebug_rs2 <= 1'b0;//**debug
          record_cnt_a_custom <= 0;
          record_cnt_total <= 0;
          rRecord_cnt_over_limit <= 1'b0;
          record_cnt_debug <= 0; //**debug
        end else begin
        
          if (StartReadDebug_rs2) begin
            if (record_cnt_debug < (RecordNumLimitDebug-1)) begin
              record_cnt_debug <= record_cnt_debug + 1'b1;
            end else begin
              DebugHaltAcquisition <= 1'b1;//**debug
            end
          end
          StartReadDebug_rs2 <= 1'b1;
          
          if ((LineRecCount < (LineDelaySel-1'b1)) && rUseDelayedStart) begin
            LineRecCount <= LineRecCount + 1'b1;
          end else begin
            if (HaltAcquisition && ~HaltAcquisitionDelayed) begin
              rCellCountOut <= rCellCounter;
              record_cnt_total <= record_cnt_a_custom+1'b1;
            end
            HaltAcquisitionDelayed <= HaltAcquisition;
            
            if (StartRead_rs2 && ((~rUseDelayedStart) || rSuppressedReadEN) && ((~rUseCellDetect) || (rBackgroundSeg^rCellDetectCntEN)) ) begin
                
              if ((~HaltAcquisition) && rCellDetectCnt_IsOne) rCellCounter <= rCellCounter + 1'b1;
              
              if ((~rRecord_cnt_over_limit) || ((IncludeLastCellSeg)&&(rUseCellDetect)&&(rCellDetectCntEN) && (~rCellDetectCnt_IsOne) && (~HaltAcquisition)) ) begin //Acquisition should be halted on the next record period.
                record_cnt_a_custom <= record_cnt_a_custom + 1'b1; //increment 1 record period after StartRead_rs2 becomes 1.
              end else begin    
                HaltAcquisition <= 1'b1;
              end

              StartedReading <= 1'b1;
            end                      
            
            DelayedStartOfFrame_rs2 <= (~StartRead_rs2); //create a delayed start of frame pulse that last for 1 record period
            StartRead_rs2 <= 1'b1; 
          end
        end

      end

      if (~(rNoRecordNumLimit || (record_cnt_a_custom < (RecordNumLimit-1)))) rRecord_cnt_over_limit <= 1'b1;        

      //*********************************************************************

      rdata_diff <= wdata_diff;
      rdata_abs_diff <= wdata_abs_diff;

      //********************** Modify control signal for discarding data records **************************************             
      if (UseRecordNumLimitDebug) begin  
        if ((~DebugHaltAcquisition) && StartReadDebug_rs2) begin
          record_bits_a_out <= record_bits_a_in[3];
          data_valid_a_out <= data_valid_a_in[3];
        end else begin
          record_bits_a_out <= 2'd00;
          data_valid_a_out <=  1'b0;               
        end
      end else begin   
        if ((~HaltAcquisition) && StartRead_rs2 && ((~rUseDelayedStart) || rSuppressedStartRead) && ((~rUseCellDetect) || (rBackgroundSeg^rCellExistWithinMargin)) ) begin
          record_bits_a_out <= record_bits_a_in[3];
          data_valid_a_out <= data_valid_a_in[3];
        end else begin
          record_bits_a_out <= 2'd00;
          data_valid_a_out <=  1'b0;               
        end          
      end  

    end
    
    //********************** Generate signal pipelines **************************************
    data_valid_a_in[1:BUS_PIPELINE] <= {data_valid_a_comb,data_valid_a_in[1:(BUS_PIPELINE-1)]};
    
    begin: gen_pipeline
      integer t;
      record_bits_a_in[1] <= record_bits_a_comb; //pipeline reg #1 from comb input
      for(t=2; t<=BUS_PIPELINE; t=t+1) record_bits_a_in[t] <= record_bits_a_in[t-1]; //pipeline reg #2 to #BUS_PIPELINE                   

      record_cnt_a_in[1] <= record_cnt_a_comb; //pipeline reg #1 from comb input
      for(t=2; t<=BUS_PIPELINE; t=t+1) record_cnt_a_in[t] <= record_cnt_a_in[t-1]; //pipeline reg #2 to #BUS_PIPELINE
    end  
      
  end  //always @ (posedge s_axis_aclk)



  generate
    for (gi=0; gi<SPD_PARALLEL_SAMPLES; gi=gi+1) begin: gen_test_pattern
      assign data_a_x[gi*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS] = pattern_cnt + gi[4:0];
    end
  endgenerate

  // *************************************************************************

  // Mux to select data or testpattern
  // BUS_PIPELINE = 2
  always @ (posedge s_axis_aclk)
  begin

    data_a_x2 <= data_a_x; //test pattern aligned to record bits
    data_a_x3 <= data_a_x2; //test pattern aligned to record bits

    data_a_in1  <= data_a_comb;
    data_a_in2 <= data_a_in1;
    data_a_in3 <= data_a_in2;
    rdata_a_buf <= wdata_a_buf;

    case (mode)  
      4'd1: data_a_out <= data_a_x3; // Test Pattern
      4'd2: data_a_out <= rdata_a_buf;// Segmented Cell
      4'd3: data_a_out <= rdata_diff; // Difference map (for debug) 
      4'd4: data_a_out <= rdata_abs_diff; //ABS of Difference map (for debug)
      4'd5: data_a_out <= rdata_detected; //Cell detection indicator (for debug)
      default: data_a_out <= data_a_in3; //"s"
    endcase
 

    // if (DebugDetect) begin
    //   if (record_bits_a_in[BUS_PIPELINE-1][0]) begin
    //     data_a_out <= 0;
    //     data_a_out[0*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,StartedReading,StartRead_rs2, 9'd0} }};
    //     data_a_out[4*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,rCellExistWithinMargin, 10'd0} }};
    //     data_a_out[8*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,rSuppressedStartRead, 10'd0} }};
        
    //     data_a_out[12*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{rCellCounter[SPD_DATAWIDTH_BITS-1:0]}};
    //     data_a_out[16*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{rCellCountOut[SPD_DATAWIDTH_BITS-1:0]}};
    //     data_a_out[20*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{record_cnt_total[SPD_DATAWIDTH_BITS-1:0]}};
    //     data_a_out[24*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ { {SPD_DATAWIDTH_BITS-MarginLineNumWidth{1'b0}},rContinuousDetectCntDn} }};
    //     data_a_out[28*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{rCellCounterContinuous[SPD_DATAWIDTH_BITS-1:0]}};
    //   end

    //   if (record_bits_a_in[BUS_PIPELINE-1][1]) begin
    //     data_a_out <= 0;
    //     data_a_out[0*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,StartedReading,StartRead_rs2, 9'd0} }};
    //     data_a_out[4*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,rCellExistWithinMargin, 10'd0} }};
    //     data_a_out[8*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,rSuppressedStartRead, 10'd0} }};
    //     data_a_out[12*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0,rSuppressStartEdgeCell, 10'd0} }};
    //     data_a_out[16*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ { {SPD_DATAWIDTH_BITS-MarginLineNumWidth{1'b0}}, rCellDetectCntDn} }};
    //     data_a_out[20*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{ {5'd0, HaltAcquisition, DelayedStartOfFrame_rs2, 9'd0} }};
    //     data_a_out[24*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{record_cnt_a_custom[SPD_DATAWIDTH_BITS-1:0]}};
    //     data_a_out[28*SPD_DATAWIDTH_BITS+:SPD_DATAWIDTH_BITS*4] <= {4{record_cnt_debug[SPD_DATAWIDTH_BITS-1:0]}};
    //   end             
    // end
     
  end


  //******** User inserting into bus output ***********************************
    //NOTE: Data valid must be asserted in all channes simultaneously when running raw streaming (e.g. without headers)
    //It is possible to set a channel mask via the ADQAPI to read out a subset of the channels, so even if data valid is asserted data can still be discarded.       
  always@(*) begin
    init_bus_output();

    // **** Note: Non-inserted signals will automatically be addded by a macro.
    // !!**** They will be delayed by the value defined by BUS_PIPELINE. ****!!

    //******** Insert data channel(s) *************
    insert_ch_all(data_a_out, CH_A);

    //******** Insert control signals *************
    insert_record_bits(record_bits_a_out, CH_A);
    insert_data_valid(data_valid_a_out, CH_A);

    finish_bus_output();
  end

   //******** GPIO ***************************
   assign gpio_in_o   = gpio_in_i;
   assign gpio_out_o  = gpio_out_i;
   assign gpio_dir_o  = gpio_dir_i;

   assign gpdi_in_o  = gpdi_in_i;
   assign gpdo_out_o = gpdo_out_i;

   assign gpio_trig_in_o = gpio_trig_in_i;
   assign gpio_trig_out_o = gpio_trig_out_i;
   assign gpio_trig_dir_o = gpio_trig_dir_i;

   assign gpio_sync_in_o = gpio_sync_in_i;
   assign gpio_sync_out_o = gpio_sync_out_i;
   assign gpio_sync_dir_o = gpio_sync_dir_i;


   // DRAM port outputs must be set to zero if unused
   assign write1_data_o = 0;
   assign write1_empty_o = 0;
   assign write1_first_addr_o = 0;
   assign write1_last_addr_o = 0;
   assign write1_last_o = 0;
   assign write1_reset_o = 0;
   assign write1_strobe_o = 0;

   assign write2_data_o = 0;
   assign write2_empty_o = 0;
   assign write2_first_addr_o = 0;
   assign write2_last_addr_o = 0;
   assign write2_last_o = 0;
   assign write2_reset_o = 0;
   assign write2_strobe_o = 0;

   assign read1_abort_o = 0;
   assign read1_afull_o = 0;
   assign read1_first_addr_o = 0;
   assign read1_high_addr_o = 0;
   assign read1_last_addr_o = 0;
   assign read1_low_addr_o = 0;
   assign read1_reset_o = 0;
   assign read1_strobe_o = 0;

   assign read2_abort_o = 0;
   assign read2_afull_o = 0;
   assign read2_first_addr_o = 0;
   assign read2_high_addr_o = 0;
   assign read2_last_addr_o = 0;
   assign read2_low_addr_o = 0;
   assign read2_reset_o = 0;
   assign read2_strobe_o = 0;

endmodule

`default_nettype wire
