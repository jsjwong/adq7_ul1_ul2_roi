/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description: CDC synchronization module for a single bit.
 */

`timescale 1 ns / 1 ps
`default_nettype none

module ul_cdc_sync #(
   NOF_STAGES = 3,
   INIT       = 1'b0
)(
   input wire clk_i,
   input wire data_i,
   output wire sync_o
);

   (* ASYNC_REG = "true" *) reg [NOF_STAGES-1:0]
      sync_sreg = {(NOF_STAGES){INIT}};

   always @(posedge clk_i)
      sync_sreg <= {sync_sreg[NOF_STAGES-2:0], data_i};

   assign sync_o = sync_sreg[NOF_STAGES-1];

endmodule

`default_nettype wire
