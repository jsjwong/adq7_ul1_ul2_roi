/* -*- coding: us-ascii-dos -*-
 *
 * Copyright Signal Processing Devices Sweden AB. All rights reserved.
 * See document "08-0175 EULA" for specific license terms regarding this file.
 *
 * Description   : Bus extraction macros, real time
 * Documentation :
 *
 */

/* Must be defined by user before including this file
localparam           BUS_PIPELINE = 1;
localparam           RT_DATA_BUS_WIDTH = 199;

wire                      s_axis_aclk;
wire [RT_DATA_BUS_WIDTH-1:0] s_axis_tdata
wire                      s_axis_tvalid;
reg [RT_DATA_BUS_WIDTH-1:0]  m_axis_tdata
reg                       m_axis_tvalid
*/

// RT Bus format:
// DATA = $SPD_PARALLEL_SAMPLES * $SPD_DATAWIDTH_BITS
// CHT  = RT_CH_TRIG_VECTOR_1_WIDTH = $RT_SPD_NUM_CH_TRIG_BITS  + $RT_SPD_NUM_TRIG_ADDBITS
// AUX  = $RT_SPD_NUM_AUX_TRIG_BITS + $RT_SPD_NUM_TRIG_ADDBITS
// TS   = $RT_SPD_TIMESTAMP_WIDTH_BITS
// OR   = OVER RANGE bit
// BUS =  $SPD_ANALOG_CHANNELS * {$OR, $CHT, $DATA}, AUX, TS

`include "bus_splitter_rt_param.vh"

// Output
`ifndef DISABLE_OUTPUT_RT
wire [RT_DATA_BUS_WIDTH:0] user_bus_o_default;
reg  [RT_DATA_BUS_WIDTH:0] user_bus_o;

generate
   reg [RT_DATA_BUS_WIDTH:0] bus_pipeline[BUS_PIPELINE-1:0];

   if (BUS_PIPELINE > 0)
     begin
        always@(posedge s_axis_aclk)
          begin : bus_pipeline_inst
             integer i;
             bus_pipeline[0][RT_DATA_BUS_WIDTH]      <= s_axis_tvalid;
             bus_pipeline[0][RT_DATA_BUS_WIDTH-1:0]  <= s_axis_tdata;
             if (BUS_PIPELINE > 1)
               for (i=0; i<BUS_PIPELINE-1; i=i+1)
                 bus_pipeline[i+1]   <= bus_pipeline[i];

          end
     end
   else
     ; //ERROR

   assign user_bus_o_default = bus_pipeline[BUS_PIPELINE-1];
endgenerate

task init_bus_output;
   begin
      user_bus_o = user_bus_o_default;
   end
endtask

task finish_bus_output;
   begin
      m_axis_tdata  = user_bus_o[RT_DATA_BUS_WIDTH-1:0];
      m_axis_tvalid = user_bus_o[RT_DATA_BUS_WIDTH];
   end
endtask

task insert_timestamp;
   input [RT_SPD_TIMESTAMP_WIDTH_BITS-1:0] timestamp;
   user_bus_o[RT_POS_TS +: RT_SPD_TIMESTAMP_WIDTH_BITS] = timestamp;
endtask

task insert_trig_inhibit;
   input [RT_SPD_TRIGGER_INHIBIT_BITS-1:0] trig_inhibit;
   user_bus_o[RT_POS_TRIGINH +: RT_SPD_TRIGGER_INHIBIT_BITS] = trig_inhibit;
endtask

task insert_timestampsync;
   input [RT_SPD_TRIGGER_INHIBIT_BITS-1:0] trig_inhibit;
   user_bus_o[RT_POS_TSSYNC +: RT_SPD_TSSYNC_BITS] = trig_inhibit;
endtask

// Aux trigger
task insert_aux_trig;
   input [RT_AUX_TRIG_VECTOR_WIDTH-1:0] vector;
   user_bus_o[RT_POS_AUX_TRIG +: RT_AUX_TRIG_VECTOR_WIDTH] = vector;
endtask

task insert_aux_trig_rnum;
  input [RT_CH_TRIG_NUM_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_RNUM_POS +: RT_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_aux_trig_rrising;
  input [RT_CH_TRIG_RISING_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_RRISING_POS +: RT_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_aux_trig_revent;
  input [RT_CH_TRIG_EVENT_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_REVENT_POS +: RT_CH_TRIG_EVENT_WIDTH] = vector;
endtask

task insert_aux_trig_tnum;
  input [RT_CH_TRIG_NUM_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_TNUM_POS +: RT_CH_TRIG_NUM_WIDTH] = vector;
endtask

task insert_aux_trig_trising;
  input [RT_CH_TRIG_RISING_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_TRISING_POS +: RT_CH_TRIG_RISING_WIDTH] = vector;
endtask

task insert_aux_trig_tevent;
  input [RT_CH_TRIG_EVENT_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_TEVENT_POS +: RT_CH_TRIG_EVENT_WIDTH] = vector;
endtask


task insert_aux_trig_revent_pt;
  input [RT_CH_TRIG_EVENT_WIDTH-1:0] vector;
  user_bus_o[RT_POS_AUX_TRIG + RT_CH_TRIG_REVENT_PT_POS +: RT_CH_TRIG_EVENT_WIDTH] = vector;
endtask

// Gate count
task insert_timestamp_sync_cnt;
   input [RT_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] vector;
   user_bus_o[RT_POS_TIMESTAMP_SYNC_CNT +: RT_SPD_TIMESTAMP_SYNC_CNT_WIDTH] = vector;
endtask

// Channel data
task insert_ch_all;
  input [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH +: SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS] = data;
endtask

// Channel trigger
task insert_ch_trig_vector;
  input [RT_CH_TRIG_VECTOR_1_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG +: RT_CH_TRIG_VECTOR_1_WIDTH] = data;
endtask

// Channel trig fields
task insert_ch_trig_rnum;
  input [RT_CH_TRIG_NUM_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_RNUM_POS +: RT_CH_TRIG_NUM_WIDTH] = data;
endtask

task insert_ch_trig_rrising;
  input [RT_CH_TRIG_RISING_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_RRISING_POS +: RT_CH_TRIG_RISING_WIDTH] = data;
endtask

task insert_ch_trig_revent;
  input [RT_CH_TRIG_EVENT_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_REVENT_POS +: RT_CH_TRIG_EVENT_WIDTH] = data;
endtask

task insert_ch_trig_tnum;
  input [RT_CH_TRIG_NUM_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_TNUM_POS +: RT_CH_TRIG_NUM_WIDTH] = data;
endtask

task insert_ch_trig_trising;
  input [RT_CH_TRIG_RISING_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_TRISING_POS +: RT_CH_TRIG_RISING_WIDTH] = data;
endtask


task insert_ch_trig_tevent;
  input [RT_CH_TRIG_EVENT_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_TEVENT_POS +: RT_CH_TRIG_EVENT_WIDTH] = data;
endtask

task insert_ch_trig_revent_pt;
  input [RT_CH_TRIG_EVENT_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_REVENT_PT_POS +: RT_CH_TRIG_EVENT_WIDTH] = data;
endtask

// Overrange
task insert_over_range;
  input data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_OVER_RANGE +: RT_SPD_OVER_RANGE_1_WIDTH] = data;
endtask

// General-purpose vector
task insert_general_purpose_vector;
  input [RT_SPD_GENERAL_PURPOSE_WIDTH-1:0] data;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    user_bus_o[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_GP_BITS +: RT_SPD_GENERAL_PURPOSE_WIDTH] = data;
endtask

`endif

//Input
`ifndef DISABLE_INPUT_RT

wire [RT_DATA_BUS_WIDTH:0] user_bus_i = {s_axis_tvalid, s_axis_tdata};

function [RT_SPD_TIMESTAMP_WIDTH_BITS-1:0] extract_timestamp;
   input dummy;
   extract_timestamp =  user_bus_i[RT_POS_TS +: RT_SPD_TIMESTAMP_WIDTH_BITS];
endfunction

function [RT_SPD_TRIGGER_INHIBIT_BITS-1:0] extract_trig_inhibit;
   input dummy;
   extract_trig_inhibit =  user_bus_i[RT_POS_TRIGINH +: RT_SPD_TRIGGER_INHIBIT_BITS];
endfunction

function [RT_SPD_TSSYNC_BITS-1:0] extract_timestampsync;
   input dummy;
   extract_timestampsync =  user_bus_i[RT_POS_TSSYNC +: RT_SPD_TSSYNC_BITS];
endfunction

// Aux trig
function [RT_AUX_TRIG_VECTOR_WIDTH-1:0] extract_aux_trig;
   input integer        dummy;
   extract_aux_trig =   user_bus_i[RT_POS_AUX_TRIG +: RT_AUX_TRIG_VECTOR_WIDTH];
endfunction

function [RT_CH_TRIG_NUM_WIDTH-1:0] extract_aux_trig_rnum;
   input integer        dummy;
   extract_aux_trig_rnum = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_RNUM_POS +: RT_CH_TRIG_NUM_WIDTH];
endfunction
function [RT_CH_TRIG_RISING_WIDTH-1:0] extract_aux_trig_rrising;
   input integer        dummy;
   extract_aux_trig_rrising = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_RRISING_POS +: RT_CH_TRIG_RISING_WIDTH];
endfunction
function [RT_CH_TRIG_EVENT_WIDTH-1:0] extract_aux_trig_revent;
   input integer        dummy;
   extract_aux_trig_revent = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_REVENT_POS +: RT_CH_TRIG_EVENT_WIDTH];
endfunction
function [RT_CH_TRIG_NUM_WIDTH-1:0] extract_aux_trig_tnum;
   input integer        dummy;
   extract_aux_trig_tnum = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_TNUM_POS +: RT_CH_TRIG_NUM_WIDTH];
endfunction
function [RT_CH_TRIG_RISING_WIDTH-1:0] extract_aux_trig_trising;
   input integer        dummy;
   extract_aux_trig_trising = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_TRISING_POS +: RT_CH_TRIG_RISING_WIDTH];
endfunction
function [RT_CH_TRIG_EVENT_WIDTH-1:0] extract_aux_trig_tevent;
   input integer        dummy;
   extract_aux_trig_tevent = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_TEVENT_POS +: RT_CH_TRIG_EVENT_WIDTH];
endfunction
function [RT_CH_TRIG_EVENT_WIDTH-1:0] extract_aux_trig_revent_pt;
   input integer        dummy;
   extract_aux_trig_revent_pt = user_bus_i[RT_POS_AUX_TRIG + RT_CH_TRIG_REVENT_PT_POS +: RT_CH_TRIG_EVENT_WIDTH];
endfunction

// Gate-count
function [RT_SPD_TIMESTAMP_SYNC_CNT_WIDTH-1:0] extract_timestamp_sync_cnt;
   input dummy;
   extract_timestamp_sync_cnt =  user_bus_i[RT_POS_TIMESTAMP_SYNC_CNT +: RT_SPD_TIMESTAMP_SYNC_CNT_WIDTH];
endfunction

function [RT_CH_WIDTH-1:0] extract_ch;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH +: RT_CH_WIDTH];
endfunction

// Channel data

function [SPD_PARALLEL_SAMPLES*SPD_DATAWIDTH_BITS-1:0] extract_ch_all;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_all = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH +: SPD_PARALLEL_SAMPLES * SPD_DATAWIDTH_BITS];
endfunction

// Channel trigger
function [RT_CH_TRIG_VECTOR_1_WIDTH-1:0] extract_ch_trig;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG +: RT_CH_TRIG_VECTOR_1_WIDTH];
endfunction

function [RT_CH_TRIG_NUM_WIDTH-1:0] extract_ch_trig_rnum;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_rnum = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_RNUM_POS +: RT_CH_TRIG_NUM_WIDTH];
endfunction

function [RT_CH_TRIG_RISING_WIDTH-1:0] extract_ch_trig_rrising;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_rrising = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_RRISING_POS +: RT_CH_TRIG_RISING_WIDTH];
endfunction

function [RT_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_revent;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_revent = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_REVENT_POS +: RT_CH_TRIG_EVENT_WIDTH];
endfunction

function [RT_CH_TRIG_NUM_WIDTH-1:0] extract_ch_trig_tnum;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_tnum = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_TNUM_POS +: RT_CH_TRIG_NUM_WIDTH];
endfunction

function [RT_CH_TRIG_RISING_WIDTH-1:0] extract_ch_trig_trising;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_trising = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_TRISING_POS +: RT_CH_TRIG_RISING_WIDTH];
endfunction

function [RT_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_tevent;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_tevent = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_TEVENT_POS +: RT_CH_TRIG_EVENT_WIDTH];
endfunction

function [RT_CH_TRIG_EVENT_WIDTH-1:0] extract_ch_trig_revent_pt;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_ch_trig_revent_pt = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_CHTRIG + RT_CH_TRIG_REVENT_PT_POS +: RT_CH_TRIG_EVENT_WIDTH];
endfunction

// Overrange
function  extract_over_range;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_over_range = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_OVER_RANGE +: 1];
endfunction

// General-purpose vector
function  [RT_SPD_GENERAL_PURPOSE_WIDTH-1:0] extract_general_purpose_vector;
  input integer ch;
  if(ch < SPD_ANALOG_CHANNELS)
    extract_general_purpose_vector = user_bus_i[RT_CH_FIRST_BIT + ch * RT_CH_WIDTH + RT_CH_POS_GP_BITS  +: RT_SPD_GENERAL_PURPOSE_WIDTH];
endfunction

`endif
